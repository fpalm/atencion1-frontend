import axios from 'axios'

const state = {
  // For affiliates in validator
  validatorDetails: []
}

const mutations = {
  // For affiliate in validator
  UPDATE_VALIDATOR_DETAILS (state, payload) {
    state.validatorDetails = payload
  }
}

const actions = {
  // For affiliate in validator
  getValidatorDetails ({ commit }, pk) {
    axios.get('https://data.venedigital.com/api/patients/' + pk, {
      headers: {Authorization: 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViY' +
        'mU0M2VjOTI3MGY1MTNkZTBiNTZiMSIsInVzZXJuYW1lIjoib3BlcmV6Iiwicm9sZSI6ImFkbWluIiwiZW1haW' +
        'wiOiJvcGVyZXpAZ3J1cG92LmNvbS52ZSIsIm5hbWUiOiJPbWFyIiwibGFzdG5hbWUiOiJQZXJleiIsImlhdCI' +
        '6MTYwMDI3MzU4OH0.gt1HyfCjBDCCmj1zX7SQwHwGBKq7xnEcXoWo3gSQPEs',
      'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_VALIDATOR_DETAILS', response.data)
    })
  },
  deleteValidatorDetails ({ commit }) {
    commit('UPDATE_VALIDATOR_DETAILS', [])
  }
}

const getters = {
  // For affiliate in validator
  validatorDetails: state => state.validatorDetails
}

const validatorModule = {
  state,
  mutations,
  actions,
  getters
}

export default validatorModule
