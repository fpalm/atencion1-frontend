import axios from 'axios'

const state = {
  // For service
  serviceDetails: [],
  serviceTypes: {},
  serviceAddressesItemsByAffiliate: [],
  sourceItems: [], // Source of service request items
  cancellationReasons: [] // Reasons of service cancellation
}

const mutations = {
  // For service
  UPDATE_SERVICE_DETAILS (state, payload) {
    state.serviceDetails = payload
  },
  UPDATE_SERVICE_TYPES (state, payload) {
    state.serviceTypes = payload
  },
  UPDATE_SERVICE_ADDRESSES_ITEMS_BY_AFFILIATE (state, payload) {
    state.serviceAddressesItemsByAffiliate = payload
  },
  UPDATE_SOURCE_ITEMS (state, payload) {
    state.sourceItems = payload
  },
  UPDATE_CANCELLATION_REASONS (state, payload) {
    state.cancellationReasons = payload
  }
}

const actions = {
  // For service
  getServiceDetails ({ commit }, data) {
    var url = ''
    if (data.type === 1) {
      url = process.env.ROOT_API + 'api/v1/HHCs/'
    } else if (data.type === 2) {
      url = process.env.ROOT_API + 'api/v1/TMGs/'
    } else if (data.type === 3) {
      url = process.env.ROOT_API + 'api/v1/transfers/'
    } else if (data.type === 4) {
      url = process.env.ROOT_API + 'api/v1/HMDs/'
    } else if (data.type === 5) {
      url = process.env.ROOT_API + 'api/v1/HHPs/'
    } else if (data.type === 6) {
      url = process.env.ROOT_API + 'api/v1/LABs/'
    }

    axios.get(url + data.pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      let service = response.data
      if (service.length !== 0) {
        service.service_type = this.state.service.serviceTypes.find(service => service.id === data.type)
      }
      commit('UPDATE_SERVICE_DETAILS', service)
    })
  },
  editService ({ dispatch }, data) {
    var url = ''
    if (data.service_type === 1) {
      url = process.env.ROOT_API + 'api/v1/HHCs-simple/'
    } else if (data.service_type === 2) {
      url = process.env.ROOT_API + 'api/v1/TMGs-simple/'
    } else if (data.service_type === 3) {
      url = process.env.ROOT_API + 'api/v1/transfers-simple/'
    } else if (data.service_type === 4) {
      url = process.env.ROOT_API + 'api/v1/HMDs-simple/'
    } else if (data.service_type === 5) {
      url = process.env.ROOT_API + 'api/v1/HHPs-simple/'
    } else if (data.service_type === 6) {
      url = process.env.ROOT_API + 'api/v1/LABs-simple/'
    }

    return new Promise((resolve, reject) => {
      axios.patch(url + data.id, data, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        const responseData = { // TODO: change name
          pk: response.data.id,
          type: data.service_type
        }
        dispatch('getServiceDetails', responseData)
        dispatch('getAttentionDetails', response.data.attention) //  Updating attention services list
        resolve(response)
      }).catch((error) => {
        reject(error.response.data.errors)
      })
    })
  },
  getServiceTypes ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/service_types', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        commit('UPDATE_SERVICE_TYPES', response.data.results)
      })
  },
  getServiceAddressesItemsByAffiliate ({ commit }, affiliate) { // Searching by affiliate
    axios.get(process.env.ROOT_API + 'api/v1/service_addresses-by-affiliate/' + affiliate, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_SERVICE_ADDRESSES_ITEMS_BY_AFFILIATE', response.data.results)
    })
  },
  getSourceItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/service_sources', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        commit('UPDATE_SOURCE_ITEMS', response.data.results)
      })
  },
  getCancellationReasons ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/cancellation_reasons?limit=50', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        commit('UPDATE_CANCELLATION_REASONS', response.data.results)
      })
  },
  deleteServiceDetails ({ commit }) {
    commit('UPDATE_SERVICE_DETAILS', [])
  }
}

const getters = {
  // For services
  serviceDetails: state => state.serviceDetails,
  serviceTypes: state => state.serviceTypes,
  serviceAddressesItemsByAffiliate: state => state.serviceAddressesItemsByAffiliate,
  sourceItems: state => state.sourceItems,
  cancellationReasons: state => state.cancellationReasons

}

const serviceModule = {
  state,
  mutations,
  actions,
  getters
}

export default serviceModule
