import axios from 'axios'

const state = {
  // For attentions
  inProgressAttentionItems: [],
  inProgressAttentionPages: {}, // count, next, previous
  onHoldAttentionItems: [],
  onHoldAttentionPages: {}, // count, next, previous
  completedAttentionItems: [],
  completedAttentionPages: {}, // count, next, previous
  nextScheduledAttentionItems: [],
  nextScheduledAttentionPages: {}, // count, next, previous
  inVerificationAttentionItems: [],
  attentionDetails: [],
  attentionHistoryItemsByAffiliate: [],
  attentionFollowUpPages: {}, // count, next, previous
  documentTypes: [] // Types of documents attached to attention
}

const mutations = {
  // For attentions
  UPDATE_IN_PROGRESS_ATTENTION_ITEMS (state, payload) {
    state.inProgressAttentionItems = payload
  },
  UPDATE_IN_PROGRESS_ATTENTION_PAGES (state, payload) {
    state.inProgressAttentionPages = payload
  },
  UPDATE_ON_HOLD_ATTENTION_ITEMS (state, payload) {
    state.onHoldAttentionItems = payload
  },
  UPDATE_ON_HOLD_ATTENTION_PAGES (state, payload) {
    state.onHoldAttentionPages = payload
  },
  UPDATE_COMPLETED_ATTENTION_ITEMS (state, payload) {
    state.completedAttentionItems = payload
  },
  UPDATE_COMPLETED_ATTENTION_PAGES (state, payload) {
    state.completedAttentionPages = payload
  },
  UPDATE_NEXT_SCHEDULED_ATTENTION_ITEMS (state, payload) {
    state.nextScheduledAttentionItems = payload
  },
  UPDATE_NEXT_SCHEDULED_ATTENTION_PAGES (state, payload) {
    state.nextScheduledAttentionPages = payload
  },
  UPDATE_IN_VERIFICATION_ATTENTION_ITEMS (state, payload) {
    state.inVerificationAttentionItems = payload
  },
  UPDATE_ATTENTION_DETAILS (state, payload) {
    state.attentionDetails = payload
  },
  UPDATE_ATTENTION_HISTORY_ITEMS_BY_AFFILIATE (state, payload) {
    state.attentionHistoryItemsByAffiliate = payload
  },
  UPDATE_ATTENTION_FOLLOW_UP_PAGES (state, payload) {
    state.attentionFollowUpPages = payload
  },
  UPDATE_DOCUMENT_TYPES (state, payload) {
    state.documentTypes = payload
  }
}

const actions = {
  // For open attentions with at least one service in progress
  getInProgressAttentionItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions-in-progress?limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_IN_PROGRESS_ATTENTION_ITEMS', response.data)
    })
  },
  // For open attentions without services or with at least one service open or delayed
  getOnHoldAttentionItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions-open-or-delayed?limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ON_HOLD_ATTENTION_ITEMS', response.data)
    })
  },
  // For open attentions with at least one service completed (to be closed)
  getCompletedAttentionItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions-completed?limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_COMPLETED_ATTENTION_ITEMS', response.data)
    })
  },
  // For open attentions with at least one service scheduled from now
  getNextScheduledAttentionItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions-next-scheduled?status=OPEN&limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_NEXT_SCHEDULED_ATTENTION_ITEMS', response.data)
    })
  },
  // For open attentions with at least one HMD service
  getInVerificationAttentionItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions-in-verification?limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_IN_VERIFICATION_ATTENTION_ITEMS', response.data)
    })
  },
  getAttentionFollowUpPages ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions?follow_up=true', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ATTENTION_FOLLOW_UP_PAGES', response.data)
    })
  },
  getAttentionDetails ({ commit }, pk) {
    return new Promise((resolve, reject) => {
      axios.get(process.env.ROOT_API + 'api/v1/attentions/' + pk, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        commit('UPDATE_ATTENTION_DETAILS', response.data)
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  deleteAttentionDetails ({ commit }) {
    commit('UPDATE_ATTENTION_DETAILS', [])
  },
  getAttentionHistoryItemsByAffiliate ({ commit }, affiliate) { // Searching by affiliate
    axios.get(process.env.ROOT_API + 'api/v1/attentions?affiliate=' + affiliate, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ATTENTION_HISTORY_ITEMS_BY_AFFILIATE', response.data)
    })
  },
  deleteAttentionHistoryItemsByAffiliate ({ commit }) {
    commit('UPDATE_ATTENTION_HISTORY_ITEMS_BY_AFFILIATE', [])
  },
  setNewAttention ({ commit, dispatch }, data) {
    return new Promise((resolve, reject) => {
      axios.post(process.env.ROOT_API + 'api/v1/attentions-simple', data, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        window.sessionStorage.setItem('selectedAttention', response.data.id)
        dispatch('getAttentionDetails', response.data.id)
        resolve(response)
      }).catch((error) => {
        reject(error.response)
      })
    })
  },
  editAttention ({ dispatch }, data) {
    return new Promise((resolve, reject) => {
      axios.patch(process.env.ROOT_API + 'api/v1/attentions-simple/' + data.id, data, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        dispatch('getAttentionDetails', data.id)
        resolve(response)
      }).catch((error) => {
        reject(error.response.data.errors)
      })
    })
  },
  getDocumentTypes ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/document_types', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        commit('UPDATE_DOCUMENT_TYPES', response.data.results)
      })
  }
}

const getters = {
  // For attentions
  inProgressAttentionItems: state => state.inProgressAttentionItems,
  inProgressAttentionPages: state => state.inProgressAttentionPages,
  onHoldAttentionItems: state => state.onHoldAttentionItems,
  onHoldAttentionPages: state => state.onHoldAttentionPages,
  completedAttentionItems: state => state.completedAttentionItems,
  completedAttentionPages: state => state.completedAttentionPages,
  nextScheduledAttentionItems: state => state.nextScheduledAttentionItems,
  nextScheduledAttentionPages: state => state.nextScheduledAttentionPages,
  inVerificationAttentionItems: state => state.inVerificationAttentionItems,
  attentionDetails: state => state.attentionDetails,
  attentionHistoryItemsByAffiliate: state => state.attentionHistoryItemsByAffiliate,
  attentionFollowUpPages: state => state.attentionFollowUpPages,
  documentTypes: state => state.documentTypes
}

const attentionModule = {
  state,
  mutations,
  actions,
  getters
}

export default attentionModule
