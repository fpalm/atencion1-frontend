import axios from 'axios'

const state = {
  // For states
  adminFirstLevelItems: [],
  // For towns
  adminSecondLevelItems: [],
  // For regions
  adminThirdLevelItems: []
}

const mutations = {
  // For states
  UPDATE_ADMIN_FIRST_LEVEL_ITEMS (state, payload) {
    state.adminFirstLevelItems = payload
  },
  // For towns
  UPDATE_ADMIN_SECOND_LEVEL_ITEMS (state, payload) {
    state.adminSecondLevelItems = payload
  },
  // For regions
  UPDATE_ADMIN_THIRD_LEVEL_ITEMS (state, payload) {
    state.adminThirdLevelItems = payload
  }
}

const actions = {
  // For states
  getAdminFirstLevelItems ({ commit }) { // Searching states by country (Venezuela)
    axios.get(process.env.ROOT_API + 'api/v1/admin-first-level?country=239&limit=24', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ADMIN_FIRST_LEVEL_ITEMS', response.data.results)
    })
  },
  // For towns
  getAdminSecondLevelItems ({ commit }) { // TODO: add filters
    axios.get(process.env.ROOT_API + 'api/v1/admin-second-level?limit=335', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ADMIN_SECOND_LEVEL_ITEMS', response.data.results)
    })
  },
  // For regions
  getAdminThirdLevelItems ({ commit }) { // TODO: add filters
    axios.get(process.env.ROOT_API + 'api/v1/admin-third-level?limit=1145', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ADMIN_THIRD_LEVEL_ITEMS', response.data.results)
    })
  }
}

const getters = {
  // For states
  adminFirstLevelItems: state => state.adminFirstLevelItems,
  // For towns
  adminSecondLevelItems: state => state.adminSecondLevelItems,
  // For regions
  adminThirdLevelItems: state => state.adminThirdLevelItems,
  // Searching for towns by state
  getSecondLevelByFirstLevelId: (state) => (id) => {
    return state.adminSecondLevelItems.filter(adminSecondLevelItems => adminSecondLevelItems.first_level === id)
  },
  // Searching for regions by town
  getThirdLevelBySecondLevelId: (state) => (id) => {
    var thirdLevel = []
    thirdLevel = state.adminThirdLevelItems.filter(adminThirdLevelItems => adminThirdLevelItems.second_level === id)
    thirdLevel.unshift({id: -1, name: 'Parroquia Desconocida', second_level: id})
    return thirdLevel
  }
}

const locationModule = {
  state,
  mutations,
  actions,
  getters
}

export default locationModule
