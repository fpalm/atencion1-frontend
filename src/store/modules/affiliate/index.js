import axios from 'axios'

const state = {
  // For affiliate
  affiliateDetails: [],
  affiliateSimpleDetails: [],
  affiliateItems: [],
  affiliatePages: {}, // count, next, previous
  affiliateAddressesItems: [],
  affiliatePhoneNumbersItems: [],
  affiliateEmailsItems: [],
  affiliateToLinkDetails: []
}

const mutations = {
  // For affiliate
  UPDATE_AFFILIATE_DETAILS (state, payload) {
    state.affiliateDetails = payload
  },
  UPDATE_AFFILIATE_SIMPLE_DETAILS (state, payload) {
    state.affiliateSimpleDetails = payload
  },
  UPDATE_AFFILIATE_ITEMS (state, payload) {
    state.affiliateItems = payload
  },
  UPDATE_AFFILIATE_PAGES (state, payload) {
    state.affiliatePages = payload
  },
  UPDATE_AFFILIATE_ADDRESSES_ITEMS (state, payload) {
    state.affiliateAddressesItems = payload
  },
  UPDATE_AFFILIATE_PHONE_NUMBERS_ITEMS (state, payload) {
    state.affiliatePhoneNumbersItems = payload
  },
  UPDATE_AFFILIATE_EMAILS_ITEMS (state, payload) {
    state.affiliateEmailsItems = payload
  },
  UPDATE_AFFILIATE_TO_LINK_DETAILS (state, payload) {
    state.affiliateToLinkDetails = payload
  }
}

const actions = {
  // For affiliate
  getAffiliateDetails ({ commit }, pk) {
    axios.get(process.env.ROOT_API + 'api/v1/affiliates/' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_DETAILS', response.data)
    })
  },
  getAffiliateSimpleDetails ({ commit }, pk) {
    axios.get(process.env.ROOT_API + 'api/v1/affiliates-simple/' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_SIMPLE_DETAILS', response.data)
    })
  },
  deleteAffiliateDetails ({ commit }) {
    commit('UPDATE_AFFILIATE_DETAILS', [])
  },
  deleteAffiliateToLinkDetails ({ commit }) {
    commit('UPDATE_AFFILIATE_TO_LINK_DETAILS', [])
  },
  getAffiliateItems ({ commit }, pk) { // Searching by first name, last name, identification number, or contract number
    axios.get(process.env.ROOT_API + 'api/v1/affiliates?search=' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_PAGES', response.data)
      commit('UPDATE_AFFILIATE_ITEMS', response.data.results)
    })
  },
  setNewAffiliate ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.post(process.env.ROOT_API + 'api/v1/affiliates-simple', data, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  getAffiliateAddressesItems ({ commit }, pk) { // Filtering by affiliate
    axios.get(process.env.ROOT_API + 'api/v1/affiliate-addresses?Affiliate=' + pk, { // TODO: change endpoint to affiliate-addresses-not-wrong-or-old
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_ADDRESSES_ITEMS', response.data.results)
    })
  },
  getAffiliatePhoneNumbersItems ({ commit }, pk) { // Filtering by affiliate
    axios.get(process.env.ROOT_API + 'api/v1/affiliate-phone-numbers?affiliate=' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_PHONE_NUMBERS_ITEMS', response.data.results)
    })
  },
  getAffiliateEmailsItems ({ commit }, pk) { // Filtering by affiliate
    axios.get(process.env.ROOT_API + 'api/v1/affiliate-emails?affiliate=' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_EMAILS_ITEMS', response.data.results)
    })
  },
  editAffiliate ({ dispatch }, data) {
    return new Promise((resolve, reject) => {
      axios.patch(process.env.ROOT_API + 'api/v1/affiliates-simple/' + data.affiliate, data, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        dispatch('getAffiliateDetails', data.affiliate)
        resolve(response)
      }).catch((error) => {
        reject(error.response.data.errors)
      })
    })
  },
  editAffiliateToLink ({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios.patch(process.env.ROOT_API + 'api/v1/affiliates-simple/' + data.affiliate, data, {
        headers: {Authorization: 'Bearer ' + this.state.login.jwt,
          'Content-Type': 'application/json'
        }
      }).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error.response.data.errors)
      })
    })
  },
  getAffiliateToLinkDetails ({ commit }, dni) {
    axios.get(process.env.ROOT_API + 'api/v1/affiliates?dni=' + dni, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_AFFILIATE_TO_LINK_DETAILS', response.data.results)
    })
  }
}

const getters = {
  // For affiliate
  affiliateDetails: state => state.affiliateDetails,

  affiliateSimpleDetails: state => state.affiliateSimpleDetails,

  affiliateItems: state => state.affiliateItems,

  getAffiliateSimplePolicies: (state) => {
    var policies = []
    for (let i = 0; i < state.affiliateSimpleDetails.policies.length; i++) {
      policies.push(state.affiliateSimpleDetails.policies[i])
    }
    return policies
  },

  getAffiliatePolicies: (state) => {
    var policies = []
    for (let i = 0; i < state.affiliateDetails.policies.length; i++) {
      policies.push(state.affiliateDetails.policies[i].policy.code)
    }
    return policies
  },

  getAffiliatePlans: (state) => {
    var plans = []
    for (let i = 0; i < state.affiliateDetails.policies.length; i++) {
      plans.push(state.affiliateDetails.policies[i].policy.plan.name)
    }
    return plans
  },

  getAffiliateKinship: (state) => {
    var affiliates = []
    for (let i = 0; i < state.affiliateDetails.policies.length; i++) {
      affiliates.push(state.affiliateDetails.policies[i].policy.affiliates)
    }
    return affiliates
  },

  getAffiliateServices: (state) => {
    var services = []
    let k = 0
    if (state.affiliateDetails.length === 0) {
      return null
    }

    for (let i = 0; i < state.affiliateDetails.policies.length; i++) {
      for (let j = 0; j < state.affiliateDetails.policies[i].policy.plan.service_types.length; j++) {
        if (state.affiliateDetails.policies[i].policy.plan.name !== 'MigrationPlan') {
          if (state.affiliateDetails.policies[i].kinship !== 'HOLDER' || state.affiliateDetails.policies[i].policy.code.substr(state.affiliateDetails.policies[i].policy.code.length - 7) !== 'ESCOLAR' || state.affiliateDetails.policies[i].policy.affiliates.length === 1) {
            var serviceId = state.affiliateDetails.policies[i].policy.plan.service_types[j].id
            var serviceCode = String(state.affiliateDetails.policies[i].policy.plan.service_types[j].code)
            var plan = String(state.affiliateDetails.policies[i].policy.plan.name)
            var policyId = String(state.affiliateDetails.policies[i].policy.id)
            var service = serviceCode + '-' + plan
            services.push({code: serviceId, display: service, index: k, policy: policyId, isDisabled: state.affiliateDetails.policies[i].policy.plan.service_types[j].id === 5})
            k++
          }
        }
      }
    }
    return services
  },
  affiliatePages: state => state.affiliatePages,

  affiliateAddressesItems: state => state.affiliateAddressesItems,
  affiliatePhoneNumbersItems: state => state.affiliatePhoneNumbersItems,
  affiliateEmailsItems: state => state.affiliateEmailsItems,
  affiliateToLinkDetails: state => state.affiliateToLinkDetails
}

const affiliateModule = {
  state,
  mutations,
  actions,
  getters
}

export default affiliateModule
