import axios from 'axios'

const state = {
  planItems: [],
  policyDetails: [],
  validatorPlanMappingItems: []
}

const mutations = {
  UPDATE_PLAN_ITEMS (state, payload) {
    state.planItems = payload
  },
  UPDATE_POLICY_DETAILS (state, payload) {
    state.policyDetails = payload
  },
  UPDATE_VALIDATOR_PLAN_MAPPING_ITEMS (state, payload) {
    state.validatorPlanMappingItems = payload
  }
}

const actions = {
  getPlans ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/plans?ordering=name&limit=150', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_PLAN_ITEMS', response.data.results)
    })
  },
  getPolicyDetails ({ commit }, pk) {
    axios.get(process.env.ROOT_API + 'api/v1/policies/' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_POLICY_DETAILS', response.data)
    })
  },
  setNewPolicy ({ dispatch }, data) {
    axios.post(process.env.ROOT_API + 'api/v1/policies-simple', data, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      dispatch('getPolicyDetails', response.data.id)
    })
  },
  getValidatorPlanMappingItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/validator-plan-mapping?limit=65', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_VALIDATOR_PLAN_MAPPING_ITEMS', response.data.results)
    })
  }
}

const getters = {
  planItems: state => state.planItems,
  policyDetails: state => state.policyDetails,
  validatorPlanMappingItems: state => state.validatorPlanMappingItems
}

const policyModule = {
  state,
  mutations,
  actions,
  getters
}

export default policyModule
