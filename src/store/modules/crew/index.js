import axios from 'axios'

const state = {
  // For crew
  crewItems: [],
  crewTodayItems: [],
  crewDetails: [],
  mobileUnitItems: [],
  baseItems: [],
  paramedicItems: [],
  doctorItems: [],
  driverItems: [],
  coordinatorItems: [],
  crewPages: [],
  crewTodayPages: []
}

const mutations = {
  // For crew
  UPDATE_WATCH_ITEMS (state, payload) {
    state.crewItems = payload
  },
  UPDATE_TODAY_WATCH_ITEMS (state, payload) {
    state.crewTodayItems = payload
  },
  UPDATE_WATCH_DETAILS (state, payload) {
    state.crewDetails = payload
  },
  UPDATE_MOBILE_UNIT_ITEMS (state, payload) {
    state.mobileUnitItems = payload
  },
  UPDATE_BASE_ITEMS (state, payload) {
    state.baseItems = payload
  },
  UPDATE_PARAMEDIC_ITEMS (state, payload) {
    state.paramedicItems = payload
  },
  UPDATE_DOCTOR_ITEMS (state, payload) {
    state.doctorItems = payload
  },
  UPDATE_DRIVER_ITEMS (state, payload) {
    state.driverItems = payload
  },
  UPDATE_COORDINATOR_ITEMS (state, payload) {
    state.coordinatorItems = payload
  },
  UPDATE_WATCH_PAGES (state, payload) {
    state.crewPages = payload
  },
  UPDATE_TODAY_WATCH_PAGES (state, payload) {
    state.crewTodayPages = payload
  }
}

const actions = {
  // For crew
  getCrewItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/crews', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_WATCH_PAGES', response.data)
      commit('UPDATE_WATCH_ITEMS', response.data.results)
    })
  },
  getTodayCrewItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/crews-today?limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_TODAY_WATCH_PAGES', response.data)
      commit('UPDATE_TODAY_WATCH_ITEMS', response.data.results)
    })
  },
  getCrewDetails ({ commit }, pk) {
    axios.get(process.env.ROOT_API + 'api/v1/crews/' + pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_WATCH_DETAILS', response.data)
    })
  },
  getMobileUnitItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/mobile-units?limit=200', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_MOBILE_UNIT_ITEMS', response.data.results)
    })
  },
  getBaseItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/bases?limit=200', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_BASE_ITEMS', response.data.results)
    })
  },
  getParamedicItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/practitioners?is_paramedic=true&limit=200', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_PARAMEDIC_ITEMS', response.data.results)
    })
  },
  getDoctorItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/practitioners?is_medic=true&limit=200', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_DOCTOR_ITEMS', response.data.results)
    })
  },
  getDriverItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/practitioners?is_driver=true&limit=200', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_DRIVER_ITEMS', response.data.results)
    })
  },
  getCoordinatorItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/practitioners?is_coordinator=true&limit=200', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_COORDINATOR_ITEMS', response.data.results)
    })
  },
  setNewWatch ({ commit }, data) {
    axios.post(process.env.ROOT_API + 'api/v1/crews-simple', data, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    })
  }
}

const getters = {
  // For crews
  crewItems: state => state.crewItems,
  crewTodayItems: state => state.crewTodayItems,
  crewDetails: state => state.crewDetails,
  mobileUnitItems: state => state.mobileUnitItems,
  baseItems: state => state.baseItems,
  paramedicItems: state => state.paramedicItems,
  doctorItems: state => state.doctorItems,
  driverItems: state => state.driverItems,
  coordinatorItems: state => state.coordinatorItems,
  crewPages: state => state.crewPages,
  crewTodayPages: state => state.crewTodayPages
}

const crewModule = {
  state,
  mutations,
  actions,
  getters
}

export default crewModule
