import axios from 'axios'

const state = {
  // For healthcare provider addresses
  healthcareProviderAddressesItems: [],
  organizationItems: [],
  organizationWithPlanItems: [],
  organizationTypeItems: []
}

const mutations = {
  UPDATE_HEALTHCARE_PROVIDER_ADDRESSES_ITEMS (state, payload) {
    state.healthcareProviderAddressesItems = payload
  },
  UPDATE_ORGANIZATION_ITEMS (state, payload) {
    state.organizationItems = payload
  },
  UPDATE_ORGANIZATION_WITH_PLAN_ITEMS (state, payload) {
    state.organizationWithPlanItems = payload
  },
  UPDATE_ORGANIZATION_TYPE_ITEMS (state, payload) {
    state.organizationTypeItems = payload
  }
}

const actions = {
  // For states
  getHealthcareProviderAddressesItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/organization-addresses?organization__organization_type=1', { // organization_type=1 corresponds to code PROV
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_HEALTHCARE_PROVIDER_ADDRESSES_ITEMS', response.data.results)
    })
  },
  getOrganizationItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/organizations?limit=2000&ordering=name', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ORGANIZATION_ITEMS', response.data.results)
    })
  },
  getOrganizationWithPlanItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/organizations?has_plans=true&limit=100&ordering=name', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ORGANIZATION_WITH_PLAN_ITEMS', response.data.results)
    })
  },
  getOrganizationTypeItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/organization_types', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ORGANIZATION_TYPE_ITEMS', response.data.results)
    })
  }
}

const getters = {
  healthcareProviderAddressesItems: state => state.healthcareProviderAddressesItems,
  organizationItems: state => state.organizationItems,
  organizationWithPlanItems: state => state.organizationWithPlanItems,
  organizationTypeItems: state => state.organizationTypeItems
}

const organizationModule = {
  state,
  mutations,
  actions,
  getters
}

export default organizationModule
