import axios from 'axios'

const state = {
  // For crew
  attentionItems: [],
  TMGItems: [],
  HHCItems: [],
  TransferItems: [],
  HMDItems: [],
  HHPItems: [],
  coordinatorItems: [],
  crewPages: []
}

const mutations = {
  // For crew
  UPDATE_ATTENTION_REPORT_ITEMS (state, payload) {
    state.attentionItems = payload
  },
  UPDATE_TMG_REPORT_ITEMS (state, payload) {
    state.TMGItems = payload
  },
  UPDATE_HHC_REPORT_ITEMS (state, payload) {
    state.HHCItems = payload
  },
  UPDATE_HMD_REPORT_ITEMS (state, payload) {
    state.HMDItems = payload
  },
  UPDATE_TRANSFER_REPORT_ITEMS (state, payload) {
    state.TransferItems = payload
  },
  UPDATE_HHP_REPORT_ITEMS (state, payload) {
    state.HHPItems = payload
  }
}

const actions = {
  // For reports
  getAttentionReportItems ({ commit }, dateAfter, dateBefore) {
    axios.get(process.env.ROOT_API + 'api/v1/attentions-report?start_after=' + dateAfter + '&start_before=' + dateBefore, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_ATTENTION_REPORT_ITEMS', response)
    })
  }
}

const getters = {
  // For crews
  attentionItems: state => state.attentionItems,
  TMGItems: state => state.TMGItems,
  HHCItems: state => state.HHCItems,
  TransferItems: state => state.TransferItems,
  HMDItems: state => state.HMDItems,
  HHPItems: state => state.HHPItems
}

const reportModule = {
  state,
  mutations,
  actions,
  getters
}

export default reportModule
