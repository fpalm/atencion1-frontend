import axios from 'axios'

const state = {
  // For healthcare provider addresses
  userItems: []
}

const mutations = {
  UPDATE_USER_ITEMS (state, payload) {
    state.userItems = payload
  }
}

const actions = {
  // For states
  getUserItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/users?limit=400', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_USER_ITEMS', response.data.results)
    })
  }
}

const getters = {
  userItems: state => state.userItems
}

const userModule = {
  state,
  mutations,
  actions,
  getters
}

export default userModule
