import axios from 'axios'

const state = {
  authUser: {},
  authUserRoles: [],
  authenticated: false,
  jwt: localStorage.getItem('token'),
  endpoints: {
    // TODO: Remove hardcoding of dev endpoints
    obtainJWT: process.env.ROOT_API + 'api/v1/token/',
    refreshJWT: process.env.ROOT_API + 'api/v1/token/resfresh/',
    baseUrl: process.env.ROOT_API
  },
  userData: []
}

const mutations = {
  SET_AUTH_USER (state, authUser) {
    state.authUser = authUser
  },
  SET_AUTH_USER_ROLES (state, authUserRoles) {
    var roles = []
    for (var i = 0; i < authUserRoles.length; i++) {
      roles.push(authUserRoles[i].name)
    }
    state.authUserRoles = roles
  },
  SET_AUTHENTICATION (state, status) {
    state.authenticated = status
  },
  UPDATE_TOKEN (state, newToken) {
    localStorage.setItem('token', newToken)
    state.jwt = newToken
  },
  REMOVE_TOKEN (state) {
    localStorage.removeItem('token')
    state.jwt = null
  },
  UPDATE_USER (state, payload) {
    state.userData = payload
  }
}

const actions = {
  setAuthentication ({ commit }, status) {
    commit('SET_AUTHENTICATION', status)
  },
  getUserData ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/profiles?user=' + this.state.login.authUser.pk, {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_USER', response.data.results[0])
      commit('SET_AUTH_USER_ROLES', response.data.results[0].user.groups)
    })
  }
}

const getters = {
  isAuthenticated: state => state.authenticated,
  userData: state => state.userData,
  roles: state => state.authUserRoles
}

const loginModule = {
  state,
  mutations,
  actions,
  getters
}

export default loginModule
