import axios from 'axios'

const state = {
  // For healthcare provider addresses
  medicalItems: [],
  symptomItems: [],
  diagnosisItems: [],
  messageItems: [],
  examTypes: {}
}

const mutations = {
  UPDATE_MEDICAL_ITEMS (state, payload) {
    state.medicalItems = payload
  },
  UPDATE_SYMPTOM_ITEMS (state, payload) {
    state.symptomItems = payload
  },
  UPDATE_DIAGNOSIS_ITEMS (state, payload) {
    state.diagnosisItems = payload
  },
  UPDATE_MESSAGE_ITEMS (state, payload) {
    state.messageItems = payload
  },
  UPDATE_EXAM_TYPES (state, payload) {
    state.examTypes = payload
  }
}

const actions = {
  getMedicalItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/medicinal-products?limit=400&available=true', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_MEDICAL_ITEMS', response.data.results)
    })
  },
  getSymptomItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/symptoms?limit=100', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_SYMPTOM_ITEMS', response.data.results)
    })
  },
  getDiagnosisItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/diagnoses?limit=300', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_DIAGNOSIS_ITEMS', response.data.results)
    })
  },
  getMessageItems ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/questionnaire-messages?limit=65', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      commit('UPDATE_MESSAGE_ITEMS', response.data.results)
    })
  },
  getExamTypes ({ commit }) {
    axios.get(process.env.ROOT_API + 'api/v1/lab_tests?limit=600&ordering=name', {
      headers: {Authorization: 'Bearer ' + this.state.login.jwt,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        commit('UPDATE_EXAM_TYPES', response.data.results)
      })
  }
}

const getters = {
  medicalItems: state => state.medicalItems,
  symptomItems: state => state.symptomItems,
  diagnosisItems: state => state.diagnosisItems,
  messageItems: state => state.messageItems,
  getMessageByICD10Code: (state) => (code) => {
    return state.messageItems.find(message => message.ICD10_source.includes(code))
  },
  getSymptomByICD10Code: (state) => (code) => {
    return state.symptomItems.find(symptom => symptom.code === code)
  },
  getDiagnosisByICD10Code: (state) => (code) => {
    return state.diagnosisItems.find(diagnosis => diagnosis.code === code)
  },
  examTypes: state => state.examTypes
}

const medicalModule = {
  state,
  mutations,
  actions,
  getters
}

export default medicalModule
