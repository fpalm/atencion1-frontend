import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import attention from './modules/attention'
import affiliate from './modules/affiliate'
import service from './modules/service'
import login from './modules/login'
import policy from './modules/policy'
import crew from './modules/crew'
import report from './modules/report'
import location from './modules/location'
import organization from './modules/organization'
import medical from './modules/medical'
import user from './modules/user'
import validator from './modules/validator'

// Plugin for persisted state management
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

// Make Axios play nice with Django CSRF
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

export default new Vuex.Store({
  modules: {
    attention,
    affiliate,
    service,
    login,
    policy,
    crew,
    report,
    location,
    organization,
    medical,
    user,
    validator
  },
  plugins: [createPersistedState()]
})
