// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import vuetify from '@/plugins/vuetify'
import VueI18n from 'vue-i18n'
import { i18n } from '@/plugins/i18n'
import VeeValidate from 'vee-validate'
import en from 'vee-validate/dist/locale/en'
import es from 'vee-validate/dist/locale/es'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Print from 'vue-print-nb'

Vue.config.productionTip = false

Vue.use(Print)
Vue.use(VueI18n)
Vue.use(VeeValidate, {
  i18nRootKey: 'validations', // customize the root path for validation messages.
  i18n,
  dictionary: {
    en,
    es
  }
})

VeeValidate.Validator.extend('verify_dni', {
  getMessage: field => 'La cédula debe contener al menos 6 cifras.',
  validate: value => {
    var strongRegex = new RegExp('[0-9]{6,}')
    return strongRegex.test(value)
  }
})

VeeValidate.Validator.extend('verify_phone', {
  getMessage: field => 'El número telefónico debe ser válido.',
  validate: value => {
    var strongRegex = new RegExp('0(2[13456789][0-9]{8}|4[12][246][0-9]{7})')
    return strongRegex.test(value)
  }
})

VeeValidate.Validator.extend('verify_password', {
  getMessage: field => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y 1 caracter especial',
  validate: value => {
    var strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})')
    return strongRegex.test(value)
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  vuetify,
  i18n,
  components: { App },
  template: '<App/>'
})
