import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Attentions from '@/components/Attentions'
import NewAttention from '@/components/attentions/NewAttention'
import Profile from '@/components/Profile'
import Settings from '@/components/Settings'
import DeclinedServices from '@/components/DeclinedServices'
import CrewList from '@/components/CrewList'
import Reports from '@/components/Reports'
import AttentionDetails from '@/components/attentions/attention_details/AttentionDetails'
import ServiceDetails from '@/components/attentions/attention_details/attention_services/service_details/ServiceDetails'
import Affiliates from '@/components/Affiliates'
import NotFound from '@/components/NotFound'
import store from '@/store'

Vue.use(Router)

const router = new Router({
  // mode: 'history',
  routes: [
    {
      path: '/404',
      component: NotFound
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/attentions',
      name: 'Attentions',
      component: Attentions
    },
    {
      path: '/new_attention',
      name: 'NewAttention',
      component: NewAttention,
      beforeEnter: (to, from, next) => {
        if (store.getters.roles.includes('Operador') === false &&
        store.getters.roles.includes('Despachador') === false && store.getters.roles.includes('Coordinador') === false &&
        store.getters.roles.includes('Gerente') === false && store.getters.roles.includes('Director') === false) {
          next(false)
        } else {
          next()
        }
      }
    },
    {
      path: '/declined_services',
      name: 'DeclinedServices',
      component: DeclinedServices
    },
    {
      path: '/crews',
      name: 'CrewList',
      component: CrewList,
      beforeEnter: (to, from, next) => {
        if (store.getters.roles.includes('Despachador') === false && store.getters.roles.includes('Asistente') === false && store.getters.roles.includes('Coordinador') === false && store.getters.roles.includes('Director') === false && store.getters.roles.includes('Gerente') === false) {
          next(false)
        } else {
          next()
        }
      }
    },
    {
      path: '/reports',
      name: 'Reports',
      component: Reports,
      beforeEnter: (to, from, next) => {
        if (store.getters.roles.includes('Asistente') === false && store.getters.roles.includes('Coordinador') === false &&
        store.getters.roles.includes('Gerente') === false && store.getters.roles.includes('Director') === false) {
          next(false)
        } else {
          next()
        }
      }
    },
    {
      path: '/attention_details/:id',
      name: 'AttentionDetails',
      component: AttentionDetails,
      props: true
    },
    {
      path: '/service_details/:id',
      name: 'ServiceDetails',
      component: ServiceDetails,
      props: true
    },
    {
      path: '/affiliates',
      name: 'Affiliates',
      component: Affiliates
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (!store.getters.isAuthenticated && to.path !== '/') {
    next({ name: 'Login' })
  } else next()
})

export default router
