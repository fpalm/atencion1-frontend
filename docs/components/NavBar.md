# NavBar

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<NavBar>` es el responsable de desplegar la barra de
cabecera que se muestra una vez el usuario esté loggeado dentro de
la aplicación.

Contiene el menú principal de la aplicación.

Despliega la información básica del usuario. Conduce al componente
`<Profile>` y `<Logout>`

---

The `<NavBar>` component is responsible for displaying the
header that is showed once the user is logged in within
the application.

It contains the main menu of the application.

Displays basic user information. It leads to the component
`<Profile>` and `<Logout>`.

## Data

| Name                  | Type      | Description | Initial value |
| --------------------- | --------- | ----------- | ------------- |
| `notificationsLength` | `number`  |             | `1`           |
| `drawer`              | `boolean` |             | `false`       |
| `menu`                | `boolean` |             | `false`       |
| `items`               | `array`   |             | `[]`          |

## Methods

### goToProfile()

**Syntax**

```typescript
goToProfile(): void
```

### logout()

**Syntax**

```typescript
logout(): void
```

### menuItems()

**Syntax**

```typescript
menuItems(): unknow
```

