# NewOrganization

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<NewOrganization>` es usado para desplegar un formulario
para la creación de nuevas organizaciones.

`<NewOrganization>` es hijo de los componentes `<AffiliateList>` y
`<NotAffiliate>`.

---

The `<NewOrganization>` component is used to display a form
for the creation of new organizations.

`<NewOrganization>` is child of `<AffiliateList>` and `<NotAffiliate>`
components.

## Props

| Name      | Type      | Description |
| --------- | --------- | ----------- |
| `visible` | `Boolean` | &nbsp;      |

## Data

| Name       | Type      | Description | Initial value   |
| ---------- | --------- | ----------- | --------------- |
| `name`     | `string`  |             | `""`            |
| `type`     | `any`     |             | `null`          |
| `code`     | `string`  |             | `""`            |
| `vat`      | `string`  |             | `""`            |
| `codeMask` | `string`  |             | `"AAA-AAA"`     |
| `vatMask`  | `string`  |             | `"A-#########"` |
| `snackbar` | `boolean` |             | `false`         |
| `text`     | `string`  |             | `""`            |
| `timeout`  | `number`  |             | `6000`          |
| `color`    | `string`  |             | `""`            |

## Computed Properties

| Name         | Description                 |
| ------------ | --------------------------- |
| `show`       | **Dependencies:** `visible` |
| `isDisabled` | **Dependencies:** `name`    |

## Events

| Name    | Description |
| ------- | ----------- |
| `close` | &nbsp;      |

## Methods

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

### createOrganization()

**Syntax**

```typescript
createOrganization(): void
```

