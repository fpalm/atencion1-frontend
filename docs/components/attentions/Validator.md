# Validator

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Validator>` es usado para mostrar la información
concerniente a los afiliados: sus datos personales, clientes
aseguradoras, planes, contratantes obtenida desde el api del
validador de Venemergancia.

Se puede acceder a este componente desde los formularios pertenecientes
a los componentes: `<NotAffiliate>` y `<NewKinship>`.

---

The `<Validator>` component is used to display the information
concerning affiliates: personal data, customers
insurance companies, plans, sponsors, obtained from the api of
Venemergancia's validator.

This component can be accessed from the forms belonging to
to the components: `<NotAffiliate>` and `<NewKinship>`.

## Props

| Name         | Type      | Description | Default |
| ------------ | --------- | ----------- | ------- |
| `search-box` | `Boolean` |             |         |
| `term`       | `String`  |             | `""`    |

## Data

| Name                | Type      | Description | Initial value |
| ------------------- | --------- | ----------- | ------------- |
| `plan`              | `string`  |             | `""`          |
| `sponsor`           | `string`  |             | `""`          |
| `searchTerm`        | `string`  |             | `""`          |
| `found`             | `boolean` |             | `false`       |
| `isLoading`         | `boolean` |             | `false`       |
| `editingSearchTerm` | `boolean` |             | `false`       |

## Methods

### titleCase()

**Syntax**

```typescript
titleCase(string: unknow): unknow
```

### affiliateSearch()

**Syntax**

```typescript
affiliateSearch(term: unknow): void
```

### getPlan()

**Syntax**

```typescript
getPlan(patientDetails: unknow): unknow
```

### getSponsor()

**Syntax**

```typescript
getSponsor(patientDetails: unknow): void
```

