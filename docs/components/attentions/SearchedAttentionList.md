# SearchedAttentionList

**Author:** Ixhel Mejías

El componente `<SearchedAttentionList>` se utiliza para mostrar la linea de
atención que viene del resultado de la busqueda hecha por el numero de atención.

La fila contendrá información relevante sobre la atención: nombre y
apellido del afiliado, hora de llamada y servicios.

`<SearchedAttentionList>` es hijo del componente `<SearchedAttention>`.

---

The `<SearchedAttentionList>` component is used to display a line of the
attention that came as a result of a search made by nomber of attention.

The row will contain relevant information about the attention: the
affiliate's first and last names, call time, and services.

`<SearchedAttentionList>` is child of the `<SearchedAttention>` component.

## Data

| Name          | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                           |
| ------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `selectedRow` | `any`     |             | `undefined`                                                                                                                                                                                                                                                                                                                                             |
| `search`      | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                    |
| `loading`     | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                 |
| `data`        | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                    |
| `expand`      | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                 |
| `selected`    | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                    |
| `headers`     | `array`   |             | `[ {text: this.$t('number'), value: 'id'}, {text: this.$t('affiliate_first_name'), value: 'affiliate.first_name'}, {text: this.$t('affiliate_last_name'), value: 'affiliate.last_name'}, {text: this.$t('call_time'), value: 'created'}, {text: this.$t('services'), value: 'services'}, {text: '', sortable: false, value: 'actions', width: '100'} ]` |
| `attention`   | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                    |

## Computed Properties

| Name              | Description               |
| ----------------- | ------------------------- |
| `permissions`     | **Dependencies:** `roles` |
| `permissions_all` | **Dependencies:** `roles` |

## Methods

### formatCreatedDate()

**Syntax**

```typescript
formatCreatedDate(date: unknow): unknow
```

### getColor()

**Syntax**

```typescript
getColor(status: unknow): void
```

### getServiceColor()

**Syntax**

```typescript
getServiceColor(status: unknow): void
```

### showDetails()

**Syntax**

```typescript
showDetails(item: unknow): void
```

### formatCrews()

**Syntax**

```typescript
formatCrews(item: unknow): unknow
```

