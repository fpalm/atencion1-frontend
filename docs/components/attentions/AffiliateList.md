# AffiliateList

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<AffiliateList>` se utiliza para mostrar un listado de
afiliados resultantes de una búsqueda, en paneles de expansión. Este
componente permite la paginación, mostrando hasta cinco filas por página.

Cada panel contendrá información relevante sobre el afiliado: nombre, apellido,
documento de identidad, fecha de nacimiento, sexo y pólizas. Si se hace clic
sobre una póliza, el usuario puede visualizar más detalles y vincular un
afiliado a la póliza. El usuario también tendrá acceso a un botón que permite
añadir una nueva póliza a un afiliado.

`<AffiliateList>` es hijo del componente `<NewAttention>`. Para la creación
de una nueva atención, el usuario debe seleccionar un afiliado del listado.

---

The `<AffiliateList>` component is used to display a list of the affiliates
resulting from a search, in expansion panels. This component enables pagination,
showing up to five rows per page.

Each panel will contain relevant information about the affiliate: first name,
last name, DNI, birthdate, sex and policies. By clicking on a policy, the
user can view more details and link an affiliate to the policy. The user will
also have access to a button that allows adding a new policy to an affiliate.

`<AffiliateList>` is child of `<NewAttention>` component. To create a new
service, the user must select an affiliate from the list.

## Props

| Name          | Type     | Description |
| ------------- | -------- | ----------- |
| `search-term` | `String` | &nbsp;      |

## Data

| Name                   | Type      | Description | Initial value                                                                                                                                                                                                                           |
| ---------------------- | --------- | ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `selected`             | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `selectedId`           | `any`     |             | `null`                                                                                                                                                                                                                                  |
| `focusable`            | `boolean` |             | `true`                                                                                                                                                                                                                                  |
| `alert`                | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `alertAttention`       | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `picked`               | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `dialog`               | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `addPlanDialog`        | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `pagination`           | `object`  |             | `{"totalItems":{"type":"Object","value":"this.$store.getters.affiliatePages.count","raw":"this.$store.getters.affiliatePages.count"},"rowsPerPage":{"type":"number","value":5,"raw":"5"},"page":{"type":"number","value":1,"raw":"1"}}` |
| `data`                 | `array`   |             | `[]`                                                                                                                                                                                                                                    |
| `attentionData`        | `array`   |             | `[]`                                                                                                                                                                                                                                    |
| `client`               | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `plan`                 | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `sponsor`              | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `authorized_by`        | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `showOrganizationForm` | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `snackbar`             | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `text`                 | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `timeout`              | `number`  |             | `6000`                                                                                                                                                                                                                                  |
| `color`                | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `editedItem`           | `object`  |             | `{}`                                                                                                                                                                                                                                    |
| `showKinshipForm`      | `boolean` |             | `false`                                                                                                                                                                                                                                 |
| `policy`               | `object`  |             | `{}`                                                                                                                                                                                                                                    |
| `contractType`         | `string`  |             | `""`                                                                                                                                                                                                                                    |
| `contractItems`        | `array`   |             | `[ {code: 'INDIVIDUAL', display: this.$t('individual')}, {code: 'GROUP', display: this.$t('collective')} ]`                                                                                                                             |

## Computed Properties

| Name            | Description                                                                        |
| --------------- | ---------------------------------------------------------------------------------- |
| `pages`         | **Dependencies:** `pagination`                                                     |
| `filteredPlans` | **Dependencies:** `planItems`, `client`                                            |
| `formFilter`    | **Dependencies:** `organizationWithPlanItems`, `client`                            |
| `isDisabled`    | **Dependencies:** `formFilter`, `plan`, `contractType`, `authorized_by`, `sponsor` |

## Events

| Name             | Description                                                      |
| ---------------- | ---------------------------------------------------------------- |
| `alertAttention` | <br>**Arguments**<br><ul><li>**`alertAttention: any`**</li></ul> |

## Methods

### getKinship()

**Syntax**

```typescript
getKinship(kinship: unknow): void
```

### getPlan()

**Syntax**

```typescript
getPlan(pickedPlan: unknow): unknow
```

### editAffiliatePolicy()

**Syntax**

```typescript
editAffiliatePolicy(): void
```

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

### onOrganizationCreated()

**Syntax**

```typescript
onOrganizationCreated(value: unknow): void
```

### editItemPlans()

**Syntax**

```typescript
editItemPlans(item: unknow): void
```

### getSelected()

**Syntax**

```typescript
getSelected(firstName: unknow, lastName: unknow, id: unknow): void
```

### dateFormattedMomentjs()

**Syntax**

```typescript
dateFormattedMomentjs(date: unknow): unknow
```

### paginationChangeHandler()

**Syntax**

```typescript
paginationChangeHandler(pageNumber: unknow): void
```

### getData()

**Syntax**

```typescript
getData(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

### getCurrentAttention()

**Syntax**

```typescript
getCurrentAttention(): void
```

### setAttentionData()

**Syntax**

```typescript
setAttentionData(results: unknow): void
```

### deletePlan()

**Syntax**

```typescript
deletePlan(): void
```

### onLinkCreated()

**Syntax**

```typescript
onLinkCreated(value: unknow): void
```

### onLinkNotCreated()

**Syntax**

```typescript
onLinkNotCreated(value: unknow): void
```

