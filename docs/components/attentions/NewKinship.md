# NewKinship

## Props

| Name          | Type      | Description |
| ------------- | --------- | ----------- |
| `visible`     | `Boolean` |             |
| `policy-data` | `Object`  | &nbsp;      |

## Data

| Name                  | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| --------------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `hasDni`              | `boolean` |             | `true`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `items`               | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `searchPhone`         | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `searchEmail`         | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `affiliate`           | `object`  |             | `{"policies":{"type":"array","value":"[]","raw":"[]"},"first_name":{"type":"string","value":"","raw":"\"\""},"last_name":{"type":"string","value":"","raw":"\"\""},"gender":{"type":"string","value":"","raw":"\"\""},"emails":{"type":"array","value":"[]","raw":"[]"},"birthdate":{"type":"CallExpression","value":"new Date(1990, 0, 1).toISOString().substr(0, 10)","raw":"new Date(1990, 0, 1).toISOString().substr(0, 10)"},"affiliate_addresses":{"type":"array","value":"[]","raw":"[]"},"phone_numbers":{"type":"array","value":"[]","raw":"[]"},"dni_type":{"type":"string","value":"V","raw":"\"V\""},"dni":{"type":"string","value":"","raw":"\"\""},"dni_suffix":{"type":"string","value":"","raw":"\"\""},"from_validator":{"type":"string","value":"","raw":"\"\""},"authorized_by":{"type":"string","value":"","raw":"\"\""},"registered_by":{"type":"string","value":"","raw":"\"\""},"kinship":{"type":"string","value":"","raw":"\"\""}}` |
| `genderItems`         | `array`   |             | `[ {code: 'MALE', display: this.$t('male')}, {code: 'FEMALE', display: this.$t('female')} ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `dniTypeItems`        | `array`   |             | `[ 'V', 'E', 'P' ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `kinshipItems`        | `array`   |             | `[ {code: 'OFFSPRING', display: this.$t('offspring')}, {code: 'COUSIN', display: this.$t('cousin')}, {code: 'BROTHER_IN_LAW', display: this.$t('brother_in_law')}, {code: 'INLAW', display: this.$t('inlaw')}, {code: 'POLITICAL_CHILD', display: this.$t('political_child')}, {code: 'GRANDPARENT', display: this.$t('grandparent')}, {code: 'PAYER', display: this.$t('payer')}, {code: 'NEPHEW', display: this.$t('nephew')}, {code: 'SIBLING', display: this.$t('sibling')}, {code: 'UNCLE', display: this.$t('uncle')}, {code: 'PARENT', display: this.$t('parent')}, {code: 'SPOUSE', display: this.$t('spouse')}, {code: 'GRANDCHILD', display: this.$t('grandchild')}, {code: 'OTHER', display: this.$t('other')} ]`                                                                                                                                                                                                                                 |
| `phoneNumberChips`    | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `phoneMask`           | `string`  |             | `"0##########"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `dniMask`             | `string`  |             | `"########"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `emailChips`          | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `text`                | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `address`             | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `addressObject`       | `object`  |             | `{}`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `showAddressForm`     | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `descriptionLimit`    | `number`  |             | `30`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `affiliatesResults`   | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `isLoadingAffiliates` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `searchAffiliates`    | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `affiliates`          | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |

## Computed Properties

| Name                                 | Description                                                           |
| ------------------------------------ | --------------------------------------------------------------------- |
| `show`                               | **Dependencies:** `visible`                                           |
| `computedStartDateFormattedMomentjs` | **Dependencies:** `$i18n`, `affiliate`                                |
| `getAge`                             | **Dependencies:** `affiliate`                                         |
| `isDisabled`                         | **Dependencies:** `affiliate`, `phoneNumberChips`, `hasDni`, `getAge` |
| `affiliateLinkageDni`                | **Dependencies:** `policyData`                                        |
| `affiliateItems`                     | **Dependencies:** `affiliatesResults`, `descriptionLimit`             |

## Events

| Name    | Description |
| ------- | ----------- |
| `close` | &nbsp;      |

## Methods

### updatePhoneTags()

**Syntax**

```typescript
updatePhoneTags(): void
```

### updateEmailTags()

**Syntax**

```typescript
updateEmailTags(): void
```

### linkAffiliate()

**Syntax**

```typescript
linkAffiliate(): void
```

### removePhoneNumber()

**Syntax**

```typescript
removePhoneNumber(item: unknow): void
```

### removeEmail()

**Syntax**

```typescript
removeEmail(item: unknow): void
```

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

### onAddressAssigned()

**Syntax**

```typescript
onAddressAssigned(...args: unknow[]): void
```

### changeDni()

**Syntax**

```typescript
changeDni(): void
```

### fullNameAffiliate()

**Syntax**

```typescript
fullNameAffiliate(item: unknow): unknow
```

### changeFormData()

**Syntax**

```typescript
changeFormData(): void
```

### loadFormData()

**Syntax**

```typescript
loadFormData(): void
```

