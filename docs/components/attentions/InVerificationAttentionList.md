# InVerificationAttentionList

**Author:** Ixhel Mejías

El componente `<InVerificationAttentionList>` se utiliza para mostrar un listado
de todas las atenciones abiertas con al menos un servicio EMD abierto, en forma
tabular.
La tabla permite la paginación, mostrando hasta 100 filas por página.

Cada fila contendrá información relevante sobre la atención: nombre y
apellido del afiliado, hora de llamada y servicios. Desde este listado, el
usuario (dependiendo del rol) tendrá acceso a los botones que permiten
añadir un nuevo servicio y ver más detalles de la atención.

`<InVerificationAttentionList>` es hijo del componente `<Attentions>`.

---

The `<InVerificationAttentionList>` component is used to display a list of the
open attentions with at least one open HMD service, in a tabular form.
The table enables pagination, showing up to 100 rows per page.

Each row will contain relevant information about the attention: the
affiliate's first and last names, call time, and services. From this list,
the user (depending on the role) will have access to the buttons that allow
adding a new service and seeing more details of the attention.

`<InVerificationAttentionList>` is child of the `<Attentions>` component.

## Data

| Name          | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                           |
| ------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `selectedRow` | `any`     |             | `undefined`                                                                                                                                                                                                                                                                                                                                             |
| `search`      | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                    |
| `loading`     | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                 |
| `pagination`  | `object`  |             | `{"totalItems":{"type":"Object","value":"this.$store.getters.inVerificationAttentionItems.count","raw":"this.$store.getters.inVerificationAttentionItems.count"},"rowsPerPage":{"type":"number","value":100,"raw":"100"},"page":{"type":"number","value":1,"raw":"1"}}`                                                                                 |
| `data`        | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                    |
| `expand`      | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                 |
| `selected`    | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                    |
| `headers`     | `array`   |             | `[ {text: this.$t('number'), value: 'id'}, {text: this.$t('affiliate_first_name'), value: 'affiliate.first_name'}, {text: this.$t('affiliate_last_name'), value: 'affiliate.last_name'}, {text: this.$t('call_time'), value: 'created'}, {text: this.$t('services'), value: 'services'}, {text: '', sortable: false, value: 'actions', width: '100'} ]` |

## Computed Properties

| Name              | Description                    |
| ----------------- | ------------------------------ |
| `permissions`     | **Dependencies:** `roles`      |
| `permissions_all` | **Dependencies:** `roles`      |
| `pages`           | **Dependencies:** `pagination` |

## Methods

### paginationChangeHandler()

**Syntax**

```typescript
paginationChangeHandler(pageNumber: unknow): void
```

### setUpdateDataInterval()

**Syntax**

```typescript
setUpdateDataInterval(): void
```

### getData()

**Syntax**

```typescript
getData(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

### formatCreatedDate()

**Syntax**

```typescript
formatCreatedDate(date: unknow): unknow
```

### getServiceColor()

**Syntax**

```typescript
getServiceColor(status: unknow): void
```

### showDetails()

**Syntax**

```typescript
showDetails(item: unknow): void
```

### formatCrews()

**Syntax**

```typescript
formatCrews(item: unknow): unknow
```

