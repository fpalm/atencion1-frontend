# NewAttention

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<NewAttention>` se utiliza para crear una nueva atención y
agregarle un nuevo servicio. Permite la búsqueda de un afiliado y los
resultados son presentados en un listado de paneles de expansión, mediante el
componente `<AffiliateList>`. En caso de que la búsqueda no arroje ningún
resultado,
se podrá crear un nuevo afiliado a través del componente `<NotAffiliate>`.

Una vez seleccionado el afiliado, el usuario debe completar los campos del
formulario para la creación de una nueva atención: números de contacto,
motivo de consulta y observaciones. Finalmente, se presentan en chips los
servicios disponibles para el afiliado seleccionado. El formulario para
la creación de un nuevo servicio se despliega al hacer clic sobre uno de los
chips, mediante el componente `<NewService>`.

---

The `<NewAttention>` component is used to create a new attention and add a
new service to it. It allows searching for an affiliate and the results are
presented in a list of expansion panels, through the `<AffiliateList>`
component. In case the search does not return any result, a new affiliate
can be created through the `<NotAffiliate>` component.

Once the affiliate is selected, the user must complete the fields of the
new service creation form: contact numbers, reason for consultation and
observations. Finally, the services available for the selected affiliate
are presented as chips. The new service creation form is displayed by
clicking on one of the chips, through the `<NewService>` component.

## Data

| Name               | Type      | Description                                         | Initial value                                                                                                                                                                                                                                                                                                                                                                                                        |
| ------------------ | --------- | --------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `searchPhone`      | `string`  |                                                     | `""`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `ongoingAttention` | `boolean` |                                                     | `false`                                                                                                                                                                                                                                                                                                                                                                                                              |
| `attention`        | `object`  |                                                     | `{"user":{"type":"Object","value":"this.$store.state.login.authUser.pk","raw":"this.$store.state.login.authUser.pk"},"affiliate":{"type":"any","value":null,"raw":"null"},"status":{"type":"string","value":"OPEN","raw":"\"OPEN\""},"reason_for_care":{"type":"string","value":"","raw":"\"\""},"phone_numbers":{"type":"array","value":"[]","raw":"[]"},"observations":{"type":"string","value":"","raw":"\"\""}}` |
| `showAlert`        | `boolean` |                                                     | `false`                                                                                                                                                                                                                                                                                                                                                                                                              |
| `found`            | `any`     |                                                     | `null`                                                                                                                                                                                                                                                                                                                                                                                                               |
| `selected`         | `any`     |                                                     | `null`                                                                                                                                                                                                                                                                                                                                                                                                               |
| `e1`               | `number`  |                                                     | `0`                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `searchTerm`       | `string`  |                                                     | `""`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `attentionCreated` | `boolean` |                                                     | `false`                                                                                                                                                                                                                                                                                                                                                                                                              |
| `serviceType`      | `string`  |                                                     | `""`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `titles`           | `array`   |                                                     | `[ { text: this.$t('attentions'), disabled: false, href: '/#/attentions' }, { text: this.$t('new_attention'), disabled: true } ]`                                                                                                                                                                                                                                                                                    |
| `affiliateId`      | `string`  |                                                     | `""`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `phoneMask`        | `string`  | This value is set to the value emitted by the child | `"0##########"`                                                                                                                                                                                                                                                                                                                                                                                                      |
| `e6`               | `number`  |                                                     | `1`                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `snackbar`         | `boolean` |                                                     | `false`                                                                                                                                                                                                                                                                                                                                                                                                              |
| `text`             | `string`  |                                                     | `""`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `timeout`          | `number`  |                                                     | `6000`                                                                                                                                                                                                                                                                                                                                                                                                               |
| `color`            | `string`  |                                                     | `""`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `phoneNumberChips` | `array`   |                                                     | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `processing`       | `boolean` |                                                     | `false`                                                                                                                                                                                                                                                                                                                                                                                                              |

## Computed Properties

| Name         | Description                                                                       |
| ------------ | --------------------------------------------------------------------------------- |
| `isDisabled` | **Dependencies:** `selected`, `attentionCreated`, `attention`, `phoneNumberChips` |

## Methods

### updatePhoneTags()

**Syntax**

```typescript
updatePhoneTags(): void
```

### affiliateSearch()

**Syntax**

```typescript
affiliateSearch(id: unknow): void
```

### onAffiliateSelection()

Triggered when event is emitted by the child.

**Syntax**

```typescript
onAffiliateSelection(value: unknow): void
```

### onNewAffiliateSelection()

**Syntax**

```typescript
onNewAffiliateSelection(value: unknow): void
```

### onAlertAttention()

**Syntax**

```typescript
onAlertAttention(value: unknow): void
```

### createAttention()

**Syntax**

```typescript
createAttention(): void
```

### goToAttentions()

**Syntax**

```typescript
goToAttentions(): void
```

### remove()

**Syntax**

```typescript
remove(item: unknow): void
```

### getPhoneNumbers()

**Syntax**

```typescript
getPhoneNumbers(): unknow
```

### onServiceCreated()

**Syntax**

```typescript
onServiceCreated(value: unknow): void
```

### onServiceNotCreated()

**Syntax**

```typescript
onServiceNotCreated(value: unknow): void
```

### onAffiliateCreated()

**Syntax**

```typescript
onAffiliateCreated(value: unknow): void
```

