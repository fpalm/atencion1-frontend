# MiniNavBar

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<MiniNavBar>` no esta usado en el sistema, a desarrollarse a
futuro

---

The `<MiniNavBar>` component is not used in the system, to be developed in the
future

## Data

| Name          | Type      | Description | Initial value |
| ------------- | --------- | ----------- | ------------- |
| `drawer`      | `any`     |             | `null`        |
| `drawerRight` | `any`     |             | `null`        |
| `right`       | `boolean` |             | `false`       |
| `left`        | `boolean` |             | `false`       |

