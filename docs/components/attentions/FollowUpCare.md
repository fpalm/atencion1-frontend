# FollowUpCare

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<FollowUpCare>` no esta usado en el sistema, a desarrollarse a
futuro

---

The `<FollowUpCare>` component is not used in the system, to be developed in the
future

## Data

| Name         | Type      | Description | Initial value                                                                                                                                                                                                                                                 |
| ------------ | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `pagination` | `object`  |             | `{"totalItems":{"type":"Object","value":"this.$store.getters.attentionFollowUpPages.count","raw":"this.$store.getters.attentionFollowUpPages.count"},"rowsPerPage":{"type":"number","value":10,"raw":"10"},"page":{"type":"number","value":1,"raw":"1"}}`     |
| `data`       | `array`   |             | `[]`                                                                                                                                                                                                                                                          |
| `search`     | `string`  |             | `""`                                                                                                                                                                                                                                                          |
| `loading`    | `boolean` |             | `false`                                                                                                                                                                                                                                                       |
| `headers`    | `array`   |             | `[ {text: 'Número', value: 'id'}, {text: 'Nombre del Afiliado', value: 'affiliate.first_name'}, {text: 'Apellido del Afiliado', value: 'affiliate.last_name'}, {text: 'Hora de llamada', value: 'created'}, {text: 'Observaciones', value: 'observations'} ]` |

## Computed Properties

| Name    | Description                    |
| ------- | ------------------------------ |
| `pages` | **Dependencies:** `pagination` |

## Methods

### paginationChangeHandler()

**Syntax**

```typescript
paginationChangeHandler(pageNumber: unknow): void
```

### getData()

**Syntax**

```typescript
getData(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

### formatCreatedDate()

**Syntax**

```typescript
formatCreatedDate(date: unknow): unknow
```

### getColor()

**Syntax**

```typescript
getColor(status: unknow): void
```

