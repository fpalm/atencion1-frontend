# AttentionDetails

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<AttentionDetails>` se utiliza para mostrar todos
los detalles de las atenciones integrando los componentes:
`<General>`, `<Affiliate>`, `<Policy>`, `<CareHistory>`,
`<AttachedDocuments>` y `<AttentionServices>` contenidos en
la carpeta attentions > attention_details.

Se puede acceder a este componente desde: `<SearchedAttentionList>`,
`<OnHoldAttentionList>`, `<InVerificationAttentionList>`,
`<NextScheduledAttentionList>`,
y `<CompletedAttentionList>`.

---

The `<AttentionDetails>` component is used to display all
the details of the attentions integrating in the components:
`<General>`, `<Affiliate>`, `<Policy>`, `<CareHistory>`,
`<AttachedDocuments>` and `<Attention Services>` contained in
the attentions > attention_details folder.

You can access this component from: `<SearchedAttentionList>`,
`<OnHoldAttentionList>`, `<InVerificationAttentionList>`,
`<NextScheduledAttentionList>`,
and `<CompletedAttentionList>`.

## Data

| Name           | Type      | Description | Initial value                                                                                                                                                                                                        |
| -------------- | --------- | ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `tab`          | `any`     |             | `null`                                                                                                                                                                                                               |
| `panel`        | `array`   |             | `[0]`                                                                                                                                                                                                                |
| `dialog`       | `boolean` |             | `false`                                                                                                                                                                                                              |
| `reason`       | `object`  |             | `{"code":{"type":"number","value":1,"raw":"1"},"display":{"type":"CallExpression","value":"this.$t('attention_done')","raw":"this.$t('attention_done')"}}`                                                           |
| `withFollowUp` | `boolean` |             | `false`                                                                                                                                                                                                              |
| `titles`       | `array`   |             | `[ { text: this.$t('attentions'), disabled: false, href: '/#/attentions' }, { text: this.$t('attention') + ' ' + window.sessionStorage.getItem('selectedAttention'), disabled: true } ]`                             |
| `reasonItems`  | `array`   |             | `[ {code: 1, display: this.$t('attention_done')}, {code: 2, display: this.$t('attention_cancelled')}, {code: 3, display: this.$t('attention_entered_by_error')}, {code: 4, display: this.$t('attention_aborted')} ]` |

## Computed Properties

| Name                   | Description                          |
| ---------------------- | ------------------------------------ |
| `fullNameUser`         | **Dependencies:** `attentionDetails` |
| `getColor`             | **Dependencies:** `attentionDetails` |
| `affiliateStatusColor` | **Dependencies:** `affiliateDetails` |

## Methods

### getCurrentDatetime()

**Syntax**

```typescript
getCurrentDatetime(): unknow
```

### closeAttention()

**Syntax**

```typescript
closeAttention(): void
```

### confirm()

**Syntax**

```typescript
confirm(): void
```

### tabItems()

**Syntax**

```typescript
tabItems(): unknow
```

