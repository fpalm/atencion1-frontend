# PathologicalHistory

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<PathologicalHistory>` no esta usado en el sistema, a
desarrollarse a futuro

---

The `<PathologicalHistory>` component is not used in the system, to be developed
in the future

## Data

| Name           | Type      | Description | Initial value |
| -------------- | --------- | ----------- | ------------- |
| `hasSaved`     | `boolean` |             | `false`       |
| `isEditing`    | `any`     |             | `null`        |
| `allergic`     | `any`     |             | `null`        |
| `asthmatic`    | `any`     |             | `null`        |
| `cardiac`      | `any`     |             | `null`        |
| `diabetic`     | `any`     |             | `null`        |
| `smoker`       | `any`     |             | `null`        |
| `hypertensive` | `any`     |             | `null`        |

## Computed Properties

| Name          | Description               |
| ------------- | ------------------------- |
| `permissions` | **Dependencies:** `roles` |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

