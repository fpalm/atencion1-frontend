# General

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<General>` se utiliza para mostrar información
general de una atención específica, escogida a partir de una
fila dentro de los listados de atenciones en en componente `<Attentions>`.

`<General>` es hijo del componente `<AttentionsDetails>`.

---

The `<General>` component is used to display information
of a specific attention, chosen from a row within the listings
of attentions in `<Attentions>` component.

`<General>` is child of the `<AttentionsDetails>` component.

## Data

| Name            | Type      | Description | Initial value                                                                                                            |
| --------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------ |
| `searchPhone`   | `string`  |             | `""`                                                                                                                     |
| `isEditing`     | `boolean` |             | `false`                                                                                                                  |
| `model`         | `any`     |             | `null`                                                                                                                   |
| `snackbar`      | `boolean` |             | `false`                                                                                                                  |
| `text`          | `string`  |             | `""`                                                                                                                     |
| `timeout`       | `number`  |             | `6000`                                                                                                                   |
| `color`         | `string`  |             | `""`                                                                                                                     |
| `phoneMask`     | `string`  |             | `"0##########"`                                                                                                          |
| `editedItem`    | `object`  |             | `{"observations":{"type":"string","value":"","raw":"\"\""},"phoneNumberChips":{"type":"array","value":"[]","raw":"[]"}}` |
| `reasonForCare` | `string`  |             | `""`                                                                                                                     |

## Computed Properties

| Name                | Description                                   |
| ------------------- | --------------------------------------------- |
| `permissions`       | **Dependencies:** `roles`                     |
| `fullNameAffiliate` | **Dependencies:** `attentionDetails`          |
| `fullNameUser`      | **Dependencies:** `attentionDetails`          |
| `formatDate`        | **Dependencies:** `$i18n`, `attentionDetails` |
| `getColor`          | **Dependencies:** `attentionDetails`          |
| `isDisabled`        | **Dependencies:** `isEditing`, `editedItem`   |

## Methods

### updatePhoneTags()

**Syntax**

```typescript
updatePhoneTags(): void
```

### displayPhoneNumber()

**Syntax**

```typescript
displayPhoneNumber(item: unknow): unknow
```

### save()

**Syntax**

```typescript
save(): void
```

### getPhoneNumbers()

**Syntax**

```typescript
getPhoneNumbers(): unknow
```

