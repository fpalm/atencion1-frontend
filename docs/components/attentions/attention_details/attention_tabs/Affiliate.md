# Affiliate

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Affiliate>` se utiliza para mostrar y editar la información
importante
del afiliado que esta en proceso de atención.

`<Affiliate>` es hijo del componente `<AttentionsDetails>`.

---

The `<Affiliate>` component is used to display and edit the important
information
of the affiliate who is in the process of an attention.

`<Affiliate>` is child of the `<AttentionsDetails>` component.

## Data

| Name                    | Type             | Description | Initial value                                                                                |
| ----------------------- | ---------------- | ----------- | -------------------------------------------------------------------------------------------- |
| `searchEmail`           | `string`         |             | `""`                                                                                         |
| `searchPhoneNumber`     | `string`         |             | `""`                                                                                         |
| `snackbar`              | `boolean`        |             | `false`                                                                                      |
| `text`                  | `string`         |             | `""`                                                                                         |
| `timeout`               | `number`         |             | `6000`                                                                                       |
| `color`                 | `string`         |             | `""`                                                                                         |
| `hasSaved`              | `boolean`        |             | `false`                                                                                      |
| `isEditing`             | `boolean`        |             | `false`                                                                                      |
| `model`                 | `any`            |             | `null`                                                                                       |
| `age`                   | `string`         |             | `""`                                                                                         |
| `firstName`             | `string`         |             | `""`                                                                                         |
| `lastName`              | `string`         |             | `""`                                                                                         |
| `affiliateEmail`        | `string`         |             | `""`                                                                                         |
| `affiliatePhoneNumbers` | `string`         |             | `""`                                                                                         |
| `phoneMask`             | `string`         |             | `"0##########"`                                                                              |
| `genderItems`           | `array`          |             | `[ {code: 'MALE', display: this.$t('male')}, {code: 'FEMALE', display: this.$t('female')} ]` |
| `affiliateGender`       | `string`         |             | `""`                                                                                         |
| `affiliateBirthdate`    | `CallExpression` |             | `new Date(1990, 0, 1).toISOString().substr(0, 10)`                                           |
| `address`               | `string`         |             | `""`                                                                                         |
| `addressObject`         | `object`         |             | `{}`                                                                                         |
| `showAddressForm`       | `boolean`        |             | `false`                                                                                      |

## Computed Properties

| Name                    | Description                                   |
| ----------------------- | --------------------------------------------- |
| `permissions`           | **Dependencies:** `roles`                     |
| `affiliateStatusColor`  | **Dependencies:** `affiliateDetails`          |
| `fullNameAffiliate`     | **Dependencies:** `affiliateDetails`          |
| `dateFormattedMomentjs` | **Dependencies:** `$i18n`, `affiliateDetails` |
| `isDisabled`            | **Dependencies:** `isEditing`                 |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

### displayEmails()

**Syntax**

```typescript
displayEmails(item: unknow): unknow
```

### updateEmailTags()

**Syntax**

```typescript
updateEmailTags(): void
```

### updatePhoneNumberTags()

**Syntax**

```typescript
updatePhoneNumberTags(): void
```

### getEmails()

**Syntax**

```typescript
getEmails(): unknow
```

### getPhoneNumbers()

**Syntax**

```typescript
getPhoneNumbers(): unknow
```

### getAddress()

**Syntax**

```typescript
getAddress(): unknow
```

### updateAge()

**Syntax**

```typescript
updateAge(): void
```

### onAddressAssigned()

**Syntax**

```typescript
onAddressAssigned(...args: unknow[]): void
```

