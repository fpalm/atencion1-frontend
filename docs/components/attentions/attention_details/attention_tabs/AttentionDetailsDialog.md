# AttentionDetailsDialog

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<AttentionDetailsDialog>` se utiliza para mostrar los
detalles de una atención que es requerida desde el historial de atenciones
de un afiliado determinado desde el componente `<AttentionsDetails>`.

Desde aqui se accede a los componentes: `<General>`, `<Affiliate>` que se
encuentran en la carpeta attentions > attention_details > attention_tabs.
Ademas, se accede al componente
`<AttentionServices>` de la carpeta attentions > attention_details >
attention_services,
así como tambien al componente `<AttachedDocuments>` de la carpeta attentions >
attention_details

`<AttentionDetailsDialog>` es hijo del componente `<AttentionsDetails>`, que
a su vez es hijo del componente `<AffiliatesTable>`.

---

The `<AttentionDetailsDialog>` component is used to display the
details of an attention that is required from the care history
of a given affiliate from the `<AttentionsDetails>` component.

From here, you can access the components: `<General>`, `<Affiliate>` that you
are found in the attentions > attention_details > attention_tabs folder. Also,
you can access
the `<AttentionServices>` component from attentions > attention_details >
attention_services folder, and the component
`<AttachedDocuments>` from attentions > attention_details folder too.

AttentionDetailsDialog>` is a child of the AttentionsDetails>` component, which
in turn is a child of the `<AffiliatesTable>` component.

## Props

| Name      | Type      | Description |
| --------- | --------- | ----------- |
| `visible` | `Boolean` | &nbsp;      |

## Data

| Name        | Type     | Description | Initial value |
| ----------- | -------- | ----------- | ------------- |
| `activeBtn` | `number` |             | `1`           |
| `titles`    | `array`  |             | `[]`          |

## Computed Properties

| Name   | Description                 |
| ------ | --------------------------- |
| `show` | **Dependencies:** `visible` |

## Events

| Name    | Description |
| ------- | ----------- |
| `close` | &nbsp;      |

