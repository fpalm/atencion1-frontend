# Policy

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Policy>` se utiliza para mostrar información acerca de las
pólizas de un afiliado: organización, plan y beneficiarios.

`<Policy>` es hijo del componente `<AttentionsDetails>`.

---

The `<Policy>` component is used to display information about an
affiliate's policies: organization, plan and beneficiaries.

`<Policy>` is child of the `<AttentionsDetails>` component.

## Data

| Name          | Type      | Description | Initial value                                                                                                                                                                                                                                              |
| ------------- | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `loading`     | `boolean` |             | `false`                                                                                                                                                                                                                                                    |
| `pagination`  | `object`  |             | `{"totalItems":{"type":"Object","value":"this.$store.getters.affiliateDetails.count","raw":"this.$store.getters.affiliateDetails.count"},"rowsPerPage":{"type":"number","value":10,"raw":"10"},"page":{"type":"number","value":1,"raw":"1"}}`              |
| `headers`     | `array`   |             | `[ { text: this.$t('first_name'), value: 'affiliate.first_name' }, { text: this.$t('last_name'), value: 'affiliate.last_name' }, { text: this.$t('idnumber'), value: 'affiliate.dni' }, { text: this.$t('linkage'), align: 'center', value: 'kinship' } ]` |
| `data`        | `array`   |             | `[]`                                                                                                                                                                                                                                                       |
| `isEditing`   | `any`     |             | `null`                                                                                                                                                                                                                                                     |
| `model`       | `any`     |             | `null`                                                                                                                                                                                                                                                     |
| `genderItems` | `array`   |             | `[ 'Masculino', 'Femenino' ]`                                                                                                                                                                                                                              |

## Computed Properties

| Name                    | Description                                   |
| ----------------------- | --------------------------------------------- |
| `pages`                 | **Dependencies:** `pagination`                |
| `fullNameAffiliate`     | **Dependencies:** `affiliateDetails`          |
| `affiliateStatusColor`  | **Dependencies:** `affiliateDetails`          |
| `dateFormattedMomentjs` | **Dependencies:** `$i18n`, `affiliateDetails` |

## Methods

### getKinship()

**Syntax**

```typescript
getKinship(kinship: unknow): void
```

