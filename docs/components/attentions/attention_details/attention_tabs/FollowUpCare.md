# FollowUpCare

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<FollowUpCare>` no esta usado en el sistema, a desarrollarse a
futuro

---

The `<FollowUpCare>` component is not used in the system, to be developed in the
future

## Data

| Name            | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| --------------- | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `dialog`        | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `editedIndex`   | `number`  |             | `-1`                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `editedItem`    | `object`  |             | `{"type":{"type":"string","value":"","raw":"\"\""},"status":{"type":"string","value":"","raw":"\"\""},"date":{"type":"string","value":"","raw":"\"\""},"next_date":{"type":"string","value":"","raw":"\"\""},"description":{"type":"string","value":"","raw":"\"\""}}`                                                                                                                                                                         |
| `headers`       | `array`   |             | `[ { text: this.$t('number'), align: 'left', value: 'id' }, { text: this.$t('type'), sortable: false, value: 'type' }, { text: this.$t('status'), value: 'status' }, { text: this.$t('date'), sortable: true, value: 'date' }, { text: this.$t('next_date'), sortable: true, value: 'next_date' }, { text: this.$t('description'), sortable: false, value: 'description' }, { text: this.$t('actions'), sortable: false, value: 'actions' } ]` |
| `followUpItems` | `array`   |             | `[ { id: '001', type: 'Contacto Efectivo', status: 'Abierta', date: '02/02/2020', next_date: '02/29/2020', description: '-' } ]`                                                                                                                                                                                                                                                                                                               |
| `pagination`    | `object`  |             | `{"totalItems":{"type":"number","value":4,"raw":"4"},"rowsPerPage":{"type":"number","value":10,"raw":"10"},"page":{"type":"number","value":1,"raw":"1"}}`                                                                                                                                                                                                                                                                                      |

## Computed Properties

| Name          | Description                           |
| ------------- | ------------------------------------- |
| `permissions` | **Dependencies:** `roles`             |
| `formTitle`   | **Dependencies:** `editedIndex`, `$t` |
| `pages`       | **Dependencies:** `pagination`        |

## Methods

### formatDate()

**Syntax**

```typescript
formatDate(date: unknow): unknow
```

### editItem()

**Syntax**

```typescript
editItem(item: unknow): void
```

### deleteItem()

**Syntax**

```typescript
deleteItem(item: unknow): void
```

### close()

**Syntax**

```typescript
close(): void
```

### save()

**Syntax**

```typescript
save(): void
```

### paginationChangeHandler()

**Syntax**

```typescript
paginationChangeHandler(pageNumber: unknow): void
```

### getData()

**Syntax**

```typescript
getData(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

