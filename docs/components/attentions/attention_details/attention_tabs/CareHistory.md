# CareHistory

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<CareHistory>` se utiliza para mostrar en un
listado las atenciones previas al afiliado que esta en proceso de atención.

`<CareHistory>` es hijo del componente `<AttentionsDetails>`.

---

The `<CareHistory>` component is used to display in a
list the previous attentions given to the affiliate who is in
the process of an attention.

`<CareHistory>` is child of the `<AttentionsDetails>` component.

## Props

| Name             | Type     | Description |
| ---------------- | -------- | ----------- |
| `activator-type` | `Number` | &nbsp;      |

## Data

| Name                         | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                      |
| ---------------------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `loading`                    | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                            |
| `pagination`                 | `object`  |             | `{"totalItems":{"type":"Object","value":"this.$store.getters.attentionHistoryItemsByAffiliate.count","raw":"this.$store.getters.attentionHistoryItemsByAffiliate.count"},"rowsPerPage":{"type":"number","value":10,"raw":"10"},"page":{"type":"number","value":1,"raw":"1"}}`                                      |
| `data`                       | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                               |
| `headers`                    | `array`   |             | `[ { text: this.$t('number'), align: 'left', value: 'id' }, { text: this.$t('date_time'), value: 'created' }, { text: this.$t('reason_for_care'), sortable: false, value: 'reason_for_care' }, { text: this.$t('services'), sortable: false, value: 'services' }, {text: '', sortable: false, value: 'actions'} ]` |
| `showAttentionDetailsDialog` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                            |

## Computed Properties

| Name    | Description                    |
| ------- | ------------------------------ |
| `pages` | **Dependencies:** `pagination` |

## Methods

### formatCreatedDate()

**Syntax**

```typescript
formatCreatedDate(date: unknow): unknow
```

### formatServices()

**Syntax**

```typescript
formatServices(item: unknow): unknow
```

### paginationChangeHandler()

**Syntax**

```typescript
paginationChangeHandler(pageNumber: unknow): void
```

### getData()

**Syntax**

```typescript
getData(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

### getServiceColor()

**Syntax**

```typescript
getServiceColor(status: unknow): void
```

### showDetails()

**Syntax**

```typescript
showDetails(item: unknow): void
```

