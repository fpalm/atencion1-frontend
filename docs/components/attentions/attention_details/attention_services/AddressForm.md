# AddressForm

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<AddressForm>` se utiliza para agregar las direcciones que se
necesitan en la aplicación, sea para las direcciones personales de los
afiliados o para los servicios que requieren dirección de atención.

Se accede a `<AddressForm>` desde los componentes`<NewKinship>` y
`<NotAffiliate>` de la carpeta attentions, `<NewService>` de la
carpeta attention > attention_details > attention_services, `<Crew>`
y`<Affiliate>`
de la carpeta attention > attention_details > attention_services >
service_details > service_tabs.

---

The `<AddressForm>` component is used to add the addresses that are
needed in the application, either for the personal addresses of
affiliates or for services that require attention management.

The `<AddressForm>` is accessed from the components `<NewKinship>` and
`<NotAffiliate>` from the attentions folder, `<NewService>` from
attention > attention_details > attention_services, `<Crew>` and `<Affiliate>`
from folder
attention > attention_details > attention_services > service_details >
service_tabs.

## Props

| Name                     | Type      | Description |
| ------------------------ | --------- | ----------- |
| `visible`                | `Boolean` |             |
| `show-autocomplete`      | `Boolean` |             |
| `create-service-address` | `Boolean` |             |
| `default-address`        | `Object`  | &nbsp;      |

## Data

| Name             | Type     | Description | Initial value |
| ---------------- | -------- | ----------- | ------------- |
| `address`        | `any`    |             | `null`        |
| `addressLine1`   | `string` |             | `""`          |
| `addressLine2`   | `string` |             | `""`          |
| `state`          | `any`    |             | `null`        |
| `town`           | `any`    |             | `null`        |
| `region`         | `any`    |             | `null`        |
| `addressesItems` | `array`  |             | `[]`          |
| `townItems`      | `array`  |             | `[]`          |
| `regionItems`    | `array`  |             | `[]`          |

## Computed Properties

| Name         | Description                                                            |
| ------------ | ---------------------------------------------------------------------- |
| `show`       | **Dependencies:** `visible`                                            |
| `isDisabled` | **Dependencies:** `address`, `addressLine1`, `state`, `town`, `region` |

## Events

| Name    | Description |
| ------- | ----------- |
| `close` | &nbsp;      |

## Methods

### addressObject()

**Syntax**

```typescript
addressObject(item: unknow): unknow
```

### displayAddress()

**Syntax**

```typescript
displayAddress(item: unknow): unknow
```

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

### getAutocompleteAddressData()

**Syntax**

```typescript
getAutocompleteAddressData(): void
```

### getAddressesItems()

**Syntax**

```typescript
getAddressesItems(): unknow
```

### getTowns()

**Syntax**

```typescript
getTowns(): void
```

### getRegions()

**Syntax**

```typescript
getRegions(): void
```

### assignAddress()

**Syntax**

```typescript
assignAddress(): void
```

### setDefaultAddress()

**Syntax**

```typescript
setDefaultAddress(): void
```

