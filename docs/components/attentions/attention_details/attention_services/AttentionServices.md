# AttentionServices

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<AttentionServices>` se utiliza para mostrar y
crear los servicios que se necesiten dentro de una atención.

Se puede acceder a `<AttentionServices>` mediante los componentes
`<AttentionDetails>` y `<AttentionDetailsDialog>`.

---

The component `<AttentionServices>` is used to display and
create the services needed within an attention.

You can access `<AttentionServices>` through the components
`<AttentionDetails>` and `<AttentionDetailsDialog>`.

## Props

| Name             | Type     | Description |
| ---------------- | -------- | ----------- |
| `activator-type` | `Number` | &nbsp;      |

## Data

| Name                      | Type             | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                          |
| ------------------------- | ---------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `headers`                 | `array`          |             | `[ {text: this.$t('code'), align: 'left', sortable: false, value: 'code'}, {text: this.$t('type'), sortable: false, value: 'service_type.name'}, {text: this.$t('start'), sortable: true, value: 'start'}, {text: this.$t('status'), sortable: false, value: 'status'}, {text: this.$t('comments'), sortable: false, value: 'comments'}, {text: '', sortable: false, value: 'actions', width: '50'} ]` |
| `attentionId`             | `CallExpression` |             | `window.sessionStorage.getItem('selectedAttention')`                                                                                                                                                                                                                                                                                                                                                   |
| `active_tab`              | `number`         |             | `0`                                                                                                                                                                                                                                                                                                                                                                                                    |
| `attentionServices`       | `array`          |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                   |
| `attentionServicesLength` | `number`         |             | `0`                                                                                                                                                                                                                                                                                                                                                                                                    |
| `closeAttentionDialog`    | `boolean`        |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                |
| `snackbar`                | `boolean`        |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                |
| `text`                    | `string`         |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                   |
| `timeout`                 | `number`         |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                 |
| `color`                   | `string`         |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                   |

## Computed Properties

| Name                             | Description                          |
| -------------------------------- | ------------------------------------ |
| `permissions`                    | **Dependencies:** `roles`            |
| `permissions_attention_closure`  | **Dependencies:** `roles`            |
| `closeAttentionButtonIsDisabled` | **Dependencies:** `attentionDetails` |

## Methods

### showDetails()

**Syntax**

```typescript
showDetails(item: unknow): void
```

### getColor()

**Syntax**

```typescript
getColor(status: unknow): void
```

### formatStartDate()

**Syntax**

```typescript
formatStartDate(date: unknow): unknow
```

### getAttentionServices()

**Syntax**

```typescript
getAttentionServices(id: unknow): unknow
```

### countAttentionServices()

**Syntax**

```typescript
countAttentionServices(): unknow
```

### closeAttention()

**Syntax**

```typescript
closeAttention(): void
```

### confirmClosing()

**Syntax**

```typescript
confirmClosing(): void
```

