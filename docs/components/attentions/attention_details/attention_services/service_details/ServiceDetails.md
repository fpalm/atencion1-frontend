# ServiceDetails

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<ServiceDetails>` se utiliza para mostrar todos
los detalles del proceso de avance de los servicios contenidos
en las atenciones. En conjunto, este componente integra los componentes:
`<General>`, `<Medical>`, `<Crew>`, `<Requests>` y
`<Medication>` contenidos en la carpeta:
attentions > attention_details > attention_services > service_details >
service_tabs,
`<ServiceWorkflowHHC>`, `<ServiceWorkflowSimpleTripTransfer>`,
`<ServiceWorkflowRoundTripTransfer>`, `<ServiceWorkflowTMG>`,
`<ServiceWorkflowHMD>`, `<ServiceWorkflowHHP>` y `<ServiceWorkflowLAB>`
contenidos en la carpeta:
attentions > attention_details > attention_services > service_details >
service_workflows,
`<NewService>` y `<PrintService>` de la carpeta: attentions > attention_details
> attention_services.

Segun el tipo de servicio, las combinaciones de componentes interados varian.

Se puede acceder a este componente desde: `<AttentionServices>`.

---

The `<ServiceDetails>` component is used to display all
the details of the process of advance the contained services
in attentions. As a whole, this component integrates the components:
General>`, `<Medical>`, `<Crew>`, `<Requests>`, `<Questions>` and
Medication>` contained in the folder:
attentions > attention_details > attention_services > service_details >
service_tabs,
ServiceWorkflowHHC>`, ServiceWorkflowSimpleTripTransfer>`,
ServiceWorkflowRoundTripTransfer>`, ServiceWorkflowTMG>`,
ServiceWorkflowHMD>`, ServiceWorkflowHHP>` and ServiceWorkflowLAB>` contained in
the folder:
attentions > attention_details > attention_services > service_details >
service_workflows,
`<NewService>` and `<PrintService>` from the folder: attentions >
attention_details > attention_services.

Depending on the type of service, the combinations of components involved vary.

You can access this component from: `<AttentionServices>`.

## Data

| Name                   | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                       |
| ---------------------- | --------- | ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `switch1`              | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                             |
| `covered`              | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                             |
| `snackbar`             | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                             |
| `text`                 | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                |
| `timeout`              | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                              |
| `color`                | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                |
| `panel`                | `array`   |             | `[0, 1, 2]`                                                                                                                                                                                                                                                                                                                                                                         |
| `attentionId`          | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                |
| `hhcTab`               | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `tmgTab`               | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `transferTab`          | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `hmdTab`               | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `hhpTab`               | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `hhcTabs`              | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                |
| `tmgTabs`              | `array`   |             | `[ // General: 0, Diagnosis: 1, Requests: 2 { id: 0, title: '' }, { id: 1, title: this.$t('general') }, { id: 2, title: this.$t('diagnoses') }, { id: 3, title: this.$t('requests') } ]`                                                                                                                                                                                            |
| `transferTabs`         | `array`   |             | `[ // General: 0, Medical: 1, Crew: 2, Surcharges: 3, Additional (Trajectory & Administrative): 4 { id: 0, title: '' }, { id: 1, title: this.$t('general') }, { id: 2, title: this.$t('crew_and_route') }, { id: 3, title: this.$t('diagnoses') } ]`                                                                                                                                |
| `hmdTabs`              | `array`   |             | `[ // Workflow: 0, General: 1, Crew: 2, Diagnosis: 3, Delivery Note: 4 { id: 0, title: '' }, { id: 1, title: this.$t('general') }, { id: 2, title: this.$t('crew_and_route') }, { id: 3, title: this.$t('diagnoses') }, { id: 4, title: this.$t('delivery_note') } ]`                                                                                                               |
| `hhpTabs`              | `array`   |             | `[ // General: 1, Crew: 2, Diagnosis: 3, Delivery Note: 4 { id: 0, title: '' }, { id: 1, title: this.$t('general') }, { id: 2, title: this.$t('crew_and_route') }, { id: 3, title: this.$t('diagnoses') } // { id: 4, title: this.$t('delivery_note') } ]`                                                                                                                          |
| `labTabs`              | `array`   |             | `[ // Workflow: 0, General: 1, Crew: 2, Results: 3 { id: 0, title: '' }, { id: 1, title: this.$t('general') }, { id: 2, title: this.$t('crew_and_route') }, { id: 3, title: this.$t('results') } ]`                                                                                                                                                                                 |
| `titles`               | `array`   |             | `[ { text: this.$t('attentions'), disabled: false, href: '/#/attentions' }, { text: this.$t('attention') + ' ' + window.sessionStorage.getItem('selectedAttention'), disabled: true, href: '/#/attention_details/' + window.sessionStorage.getItem('selectedAttention') }, { text: this.$t('service') + ' ' + window.sessionStorage.getItem('selectedService'), disabled: true } ]` |
| `closeDialog`          | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                             |
| `cancelDialog`         | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                             |
| `reason`               | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                |
| `comments`             | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                |
| `cancellationReason`   | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                |
| `labDocumentsCount`    | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `hhcLabDocumentsCount` | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                              |
| `showProgressDialog`   | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                             |

## Computed Properties

| Name                                   | Description                                                                                       |
| -------------------------------------- | ------------------------------------------------------------------------------------------------- |
| `sendEmailValidation`                  | **Dependencies:** `serviceDetails`, `attentionDetails`                                            |
| `email_permission`                     | **Dependencies:** `roles`                                                                         |
| `getColor`                             | **Dependencies:** `serviceDetails`                                                                |
| `hasEquipment`                         | **Dependencies:** `serviceDetails`, `hasScheduledEquipmentServiceHMD`                             |
| `hasScheduledEquipmentServiceHMD`      | **Dependencies:** `attentionDetails`                                                              |
| `hasHDM`                               | **Dependencies:** `affiliateDetails`                                                              |
| `gotCancelled`                         | **Dependencies:** `serviceDetails`                                                                |
| `cancellationReasonAlert`              | **Dependencies:** `cancellationReasons`, `serviceDetails`                                         |
| `isDisabled`                           | **Dependencies:** `reason`                                                                        |
| `tmgCloseServiceButtonIsDisabled`      | **Dependencies:** `serviceDetails`                                                                |
| `hhcCloseServiceButtonIsDisabled`      | **Dependencies:** `serviceDetails`, `hhcLabDocumentsCount`                                        |
| `transferCloseServiceButtonIsDisabled` | **Dependencies:** `serviceDetails`                                                                |
| `hmdCloseServiceButtonIsDisabled`      | **Dependencies:** `serviceDetails`                                                                |
| `hhpCloseServiceButtonIsDisabled`      | **Dependencies:** `serviceDetails`                                                                |
| `labCloseServiceButtonIsDisabled`      | **Dependencies:** `serviceDetails`, `labDocumentsCount`                                           |
| `permissions_service_closure`          | **Dependencies:** `roles`                                                                         |
| `permissions_practitioner`             | **Dependencies:** `roles`                                                                         |
| `permissions_dispatcher`               | **Dependencies:** `roles`                                                                         |
| `permissions_service_creation`         | **Dependencies:** `roles`                                                                         |
| `permissions_service_printing`         | **Dependencies:** `roles`                                                                         |
| `permissions_service_cancellation`     | **Dependencies:** `permissions_practitioner`, `serviceDetails`, `permissions_dispatcher`, `roles` |
| `hhcCloseMessage`                      | **Dependencies:** `serviceDetails`, `$t`                                                          |
| `reasonItems`                          | **Dependencies:** `$t`, `serviceDetails`                                                          |

## Methods

### getCurrentDatetime()

**Syntax**

```typescript
getCurrentDatetime(): unknow
```

### closeService()

**Syntax**

```typescript
closeService(): void
```

### confirmClosing()

**Syntax**

```typescript
confirmClosing(): void
```

### cancelService()

**Syntax**

```typescript
cancelService(): void
```

### confirmCancellation()

**Syntax**

```typescript
confirmCancellation(): void
```

### getHHCTabs()

**Syntax**

```typescript
getHHCTabs(): void
```

### getLabDocumentsCount()

**Syntax**

```typescript
getLabDocumentsCount(): void
```

### setLabDocumentsCount()

**Syntax**

```typescript
setLabDocumentsCount(results: unknow): void
```

### getHHCLabDocumentsCount()

**Syntax**

```typescript
getHHCLabDocumentsCount(): void
```

### setHHCLabDocumentsCount()

**Syntax**

```typescript
setHHCLabDocumentsCount(results: unknow): void
```

### sendEmail()

**Syntax**

```typescript
sendEmail(): void
```

