# Medication

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Medication>` se utiliza para mostrar y editar información
sobre los medicamentos que se despachan en el servicio contenido
en una atención.

Este componente requiere de `<PrinMedication>` para completar su uso.

`<Medication>` es hijo del componente `<ServiceDetails>`.

---

The `<Medication>` component is used to display and edit
information related medication to be delivered in the service contained
in an attention.

This component requires `<PrinMedication>` to complete its use.

`<Medication>` is a child of the `<ServiceDetails>` component.

## Data

| Name                        | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| --------------------------- | --------- | ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `mask`                      | `string`  |             | `"##"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `dialog`                    | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `confirmDeletingItemDialog` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `editedIndex`               | `number`  |             | `-1`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `editedItem`                | `object`  |             | `{"medicament":{"type":"any","value":null,"raw":"null"},"deliveryQuantity":{"type":"any","value":null,"raw":"null"},"deliveryUnit":{"type":"string","value":"","raw":"\"\""}}`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `deletedIndex`              | `number`  |             | `-1`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `deletedItem`               | `object`  |             | `{"medicament":{"type":"object","value":{"name":{"type":"string","value":"","raw":"\"\""},"quantity":{"type":"any","value":null,"raw":"null"},"quantity_unit":{"type":"string","value":"","raw":"\"\""},"type":{"type":"string","value":"","raw":"\"\""}},"raw":"{\"name\":{\"type\":\"string\",\"value\":\"\",\"raw\":\"\\\"\\\"\"},\"quantity\":{\"type\":\"any\",\"value\":null,\"raw\":\"null\"},\"quantity_unit\":{\"type\":\"string\",\"value\":\"\",\"raw\":\"\\\"\\\"\"},\"type\":{\"type\":\"string\",\"value\":\"\",\"raw\":\"\\\"\\\"\"}}"},"deliveryQuantity":{"type":"any","value":null,"raw":"null"},"deliveryUnit":{"type":"string","value":"","raw":"\"\""}}` |
| `headers`                   | `array`   |             | `[ { text: this.$t('medicament_or_medical_supply'), sortable: false, value: 'medicament' }, { text: this.$t('quantity'), sortable: false, value: 'deliveryQuantity' }, { text: this.$t('unit'), sortable: false, value: 'deliveryUnit' }, { text: this.$t('actions'), sortable: false, value: 'actions' } ]`                                                                                                                                                                                                                                                                                                                                                                  |
| `deliveryMedicaments`       | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `isEditing`                 | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `description`               | `string`  |             | `"Ninguna"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `medicinalProducts`         | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `snackbar`                  | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `text`                      | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `timeout`                   | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `color`                     | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `acuteStock`                | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `criticalStock`             | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |

## Computed Properties

| Name                       | Description                                            |
| -------------------------- | ------------------------------------------------------ |
| `permissions`              | **Dependencies:** `roles`, `serviceDetails`            |
| `formTitle`                | **Dependencies:** `editedIndex`, `$t`                  |
| `saveMedicamentButtonText` | **Dependencies:** `editedIndex`, `$t`                  |
| `isDisabled`               | **Dependencies:** `description`, `deliveryMedicaments` |
| `isDisabledSaveMedicament` | **Dependencies:** `editedItem`                         |

## Methods

### editItem()

**Syntax**

```typescript
editItem(item: unknow): void
```

### confirmDeletingItem()

**Syntax**

```typescript
confirmDeletingItem(item: unknow): void
```

### deleteItem()

**Syntax**

```typescript
deleteItem(): void
```

### close()

**Syntax**

```typescript
close(): void
```

### saveMedicament()

**Syntax**

```typescript
saveMedicament(): void
```

### save()

**Syntax**

```typescript
save(): void
```

### appendIconMedication()

**Syntax**

```typescript
appendIconMedication(type: unknow): void
```

### medicalIconColor()

**Syntax**

```typescript
medicalIconColor(item: unknow): void
```

### getDeliveryMedicaments()

**Syntax**

```typescript
getDeliveryMedicaments(): unknow
```

### resetValidations()

fullMedicamentDisplay: item => item.name + ' ' + Number(item.quantity) + ' ' +
item.quantity_unit,

**Syntax**

```typescript
resetValidations(): void
```

