# General

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<General>` se utiliza para mostrar y editar la información
general del servicio desplegado contenido en una atención.

Este componente puede requerir `<ScheduleForm>` para completar su uso.

`<General>` es hijo del componente `<ServiceDetails>`.

---

The `<General>` component is used to display and edit the information
general of the service deployed content in an attention.

This component may require `<ScheduleForm>` to complete its use.

General>` is a child of the `<ServiceDetails>` component.

## Data

| Name               | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ------------------ | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `snackbar`         | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `text`             | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `timeout`          | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `color`            | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `isEditing`        | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `model`            | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `service`          | `object`  |             | `{"comments":{"type":"string","value":"","raw":"\"\""},"transferReason":{"type":"string","value":"","raw":"\"\""},"intensiveCareUnit":{"type":"boolean","value":false,"raw":"false"},"interinstitutional":{"type":"boolean","value":false,"raw":"false"},"interstate":{"type":"boolean","value":false,"raw":"false"},"labExams":{"type":"string","value":"","raw":"\"\""},"scheduled":{"type":"boolean","value":false,"raw":"false"},"scheduledDatetime":{"type":"string","value":"","raw":"\"\""}}` |
| `showScheduleForm` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `datetimeAssigned` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |

## Computed Properties

| Name                             | Description                                       |
| -------------------------------- | ------------------------------------------------- |
| `permissions`                    | **Dependencies:** `roles`                         |
| `permissions_service_scheduling` | **Dependencies:** `roles`                         |
| `getColor`                       | **Dependencies:** `serviceDetails`                |
| `formatStartDate`                | **Dependencies:** `$i18n`, `serviceDetails`       |
| `formatScheduledDatetime`        | **Dependencies:** `$i18n`, `service`              |
| `fullNamePetitioner`             | **Dependencies:** `serviceDetails`                |
| `fullNameAttendant`              | **Dependencies:** `serviceDetails`                |
| `serviceRequestSource`           | **Dependencies:** `serviceDetails`, `sourceItems` |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

### onDatetimeAssigned()

**Syntax**

```typescript
onDatetimeAssigned(...args: unknow[]): void
```

### translateStatus()

**Syntax**

```typescript
translateStatus(statusES: unknow): void
```

