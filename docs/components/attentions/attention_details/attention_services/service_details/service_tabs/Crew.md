# Crew

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Crew>` se utiliza para asignar y mostrar información
de la tripulación que prestará apoyo dentro del servicio.

Este componente requere de `<AddressForm>` para completar su uso.

`<Crew>` es hijo del componente `<ServiceDetails>`.

---

The `<Crew>` component is used to assign and display information
of the crew that will provide support within the service.

This component requires `<AddressForm>` to complete its use.

`<Crew>` is a child of the `<ServiceDetails>` component.

## Data

| Name                          | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                              |
| ----------------------------- | --------- | ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `snackbar`                    | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                    |
| `text`                        | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                       |
| `timeout`                     | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                     |
| `color`                       | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                       |
| `isEditing`                   | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                    |
| `model`                       | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                     |
| `origin`                      | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                       |
| `destination`                 | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                       |
| `externalProvider`            | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                     |
| `providerItems`               | `array`   |             | `[ // TODO: hook up dynamic data {code: 1, display: 'Proveedor 1'}, {code: 2, display: 'Proveedor 2'}, {code: 3, display: 'Proveedor 3'} ]`                                                                                                                                                                                                                                |
| `crew`                        | `object`  |             | `{"code":{"type":"string","value":"","raw":"\"\""},"unit":{"type":"string","value":"","raw":"\"\""},"base":{"type":"string","value":"","raw":"\"\""},"doctor":{"type":"string","value":"","raw":"\"\""},"paramedic":{"type":"string","value":"","raw":"\"\""},"driver":{"type":"string","value":"","raw":"\"\""},"coordinator":{"type":"string","value":"","raw":"\"\""}}` |
| `showAddressForm`             | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                    |
| `assignOrigin`                | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                    |
| `assignDestination`           | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                    |
| `originObject`                | `object`  |             | `{}`                                                                                                                                                                                                                                                                                                                                                                       |
| `destinationObject`           | `object`  |             | `{}`                                                                                                                                                                                                                                                                                                                                                                       |
| `defaultAddressObject`        | `object`  |             | `{}`                                                                                                                                                                                                                                                                                                                                                                       |
| `originServiceAddressId`      | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                     |
| `destinationServiceAddressId` | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                     |

## Computed Properties

| Name                        | Description                                 |
| --------------------------- | ------------------------------------------- |
| `isDisabled`                | **Dependencies:** `isEditing`               |
| `permissions_crew_edition`  | **Dependencies:** `roles`, `serviceDetails` |
| `permissions_route_edition` | **Dependencies:** `roles`, `serviceDetails` |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

### updateData()

**Syntax**

```typescript
updateData(): void
```

### getPractitioner()

**Syntax**

```typescript
getPractitioner(role: unknow): unknow
```

### onAddressAssigned()

**Syntax**

```typescript
onAddressAssigned(...args: unknow[]): void
```

