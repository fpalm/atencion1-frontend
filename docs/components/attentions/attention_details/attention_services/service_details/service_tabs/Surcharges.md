# Surcharges

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Surcharges>` no esta usado en el sistema, a desarrollarse a
futuro

---

The `<Surcharges>` component is not used in the system, to be developed in the
future

## Data

| Name        | Type      | Description | Initial value                                                                                                                                                                                                                                     |
| ----------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `hasSaved`  | `boolean` |             | `false`                                                                                                                                                                                                                                           |
| `isEditing` | `any`     |             | `null`                                                                                                                                                                                                                                            |
| `model`     | `any`     |             | `null`                                                                                                                                                                                                                                            |
| `service`   | `object`  |             | `{"transferType":{"type":"string","value":"simple-transfer","raw":"\"simple-transfer\""},"level":{"type":"string","value":"basic-level","raw":"\"basic-level\""},"oxygen":{"type":"string","value":"without-oxygen","raw":"\"without-oxygen\""}}` |

## Computed Properties

| Name          | Description               |
| ------------- | ------------------------- |
| `permissions` | **Dependencies:** `roles` |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

