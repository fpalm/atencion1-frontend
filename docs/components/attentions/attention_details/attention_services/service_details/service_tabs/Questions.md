# Questions

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Questions>` no esta usado en el sistema, a desarrollarse a
futuro

---

The `<Questions>` component is not used in the system, to be developed in the
future

## Data

| Name            | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                              |
| --------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `hasSaved`      | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `isEditing`     | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `model`         | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `singleSelect`  | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `selected`      | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `headers`       | `array`   |             | `[ { text: 'Código', align: 'left', sortable: false, value: 'code' }, { text: 'Pregunta', align: 'left', sortable: false, value: 'text' }, { text: 'Respuesta', align: 'left', sortable: false, value: 'answer' } ]`                                                                                                                                                                                                                       |
| `questionItems` | `array`   |             | `[ { code: '001', text: '¿Expulsa sangre cuando tose?', answer: false }, { code: '002', text: '¿Siente dificultad para respirar o respira más de 30 veces en 1 minuto?', answer: true }, { code: '003', text: '¿Tiene los labios o los dedos de color azul?', answer: false }, { code: '004', text: '¿Se "Ahoga" con la tos?', answer: false }, { code: '005', text: '¿Al presentarse el ataque de tos tiene vómitos?', answer: false } ]` |

## Computed Properties

| Name          | Description               |
| ------------- | ------------------------- |
| `permissions` | **Dependencies:** `roles` |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

### discardChanges()

**Syntax**

```typescript
discardChanges(): void
```

### formatAnswer()

**Syntax**

```typescript
formatAnswer(booleanAnswer: unknow): void
```

