# Requests

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Requests>` se utiliza para mostrar y editar información
sobre las solicitudes que se realicen a partir del progreso del servicio
contenido en una atención.

`<Requests>` es hijo del componente `<ServiceDetails>`.

---

The `<Requests>` component is used to display and edit information
about the requests made from the progress of the service contained in
an attention.

`<Requests>` is a child of the `<ServiceDetails>` component.

## Data

| Name        | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ----------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `request`   | `object`  |             | `{"medications":{"type":"string","value":"","raw":"\"\""},"indications":{"type":"string","value":"","raw":"\"\""},"external_services":{"type":"string","value":"","raw":"\"\""},"observations":{"type":"string","value":"","raw":"\"\""},"recommendations":{"type":"string","value":"","raw":"\"\""},"alarm_signs":{"type":"string","value":"","raw":"\"\""}}`                                                                                                                       |
| `snackbar`  | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `text`      | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `timeout`   | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `color`     | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `isEditing` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `model`     | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `printObj1` | `object`  |             | `{"id":{"type":"string","value":"printRequest1","raw":"\"printRequest1\""},"popTitle":{"type":"string","value":"Solicitudes","raw":"\"Solicitudes\""},"extraCss":{"type":"string","value":"https://www.google.com,https://www.google.com","raw":"\"https://www.google.com,https://www.google.com\""},"extraHead":{"type":"string","value":"<meta http-equiv=\"Content-Language\"content=\"es-ve\"/>","raw":"\"<meta http-equiv=\\\"Content-Language\\\"content=\\\"es-ve\\\"/>\""}}` |
| `printObj2` | `object`  |             | `{"id":{"type":"string","value":"printRequest2","raw":"\"printRequest2\""},"popTitle":{"type":"string","value":"Solicitudes","raw":"\"Solicitudes\""},"extraCss":{"type":"string","value":"https://www.google.com,https://www.google.com","raw":"\"https://www.google.com,https://www.google.com\""},"extraHead":{"type":"string","value":"<meta http-equiv=\"Content-Language\"content=\"es-ve\"/>","raw":"\"<meta http-equiv=\\\"Content-Language\\\"content=\\\"es-ve\\\"/>\""}}` |
| `printObj3` | `object`  |             | `{"id":{"type":"string","value":"printRequest3","raw":"\"printRequest3\""},"popTitle":{"type":"string","value":"Solicitudes","raw":"\"Solicitudes\""},"extraCss":{"type":"string","value":"https://www.google.com,https://www.google.com","raw":"\"https://www.google.com,https://www.google.com\""},"extraHead":{"type":"string","value":"<meta http-equiv=\"Content-Language\"content=\"es-ve\"/>","raw":"\"<meta http-equiv=\\\"Content-Language\\\"content=\\\"es-ve\\\"/>\""}}` |
| `printObj4` | `object`  |             | `{"id":{"type":"string","value":"printRequest4","raw":"\"printRequest4\""},"popTitle":{"type":"string","value":"Solicitudes","raw":"\"Solicitudes\""},"extraCss":{"type":"string","value":"https://www.google.com,https://www.google.com","raw":"\"https://www.google.com,https://www.google.com\""},"extraHead":{"type":"string","value":"<meta http-equiv=\"Content-Language\"content=\"es-ve\"/>","raw":"\"<meta http-equiv=\\\"Content-Language\\\"content=\\\"es-ve\\\"/>\""}}` |

## Computed Properties

| Name                       | Description                                     |
| -------------------------- | ----------------------------------------------- |
| `isDisabled`               | **Dependencies:** `isEditing`, `serviceDetails` |
| `permissions_practitioner` | **Dependencies:** `roles`, `serviceDetails`     |
| `permissions_dispatcher`   | **Dependencies:** `roles`, `serviceDetails`     |

## Methods

### dateFormattedMomentjs()

**Syntax**

```typescript
dateFormattedMomentjs(date: unknow): unknow
```

### save()

**Syntax**

```typescript
save(): void
```

