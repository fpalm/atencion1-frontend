# PrintMedication

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<PrintMedication>` se utiliza para mostrar e imprimir
la información sobre los medicamentos que se despachan en el servicio contenido
en una atención.

`<PrintMedication>` es hijo del componente `<Medication>`.

---

The `<PrintMedication>` component is used to display and print
information related medication to be delivered in the service contained
in an attention.

`<PrintMedication>` is a child of the `<Medication>` component.

## Data

| Name                  | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| --------------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `printObj`            | `object`  |             | `{"id":{"type":"string","value":"printMedication","raw":"\"printMedication\""},"popTitle":{"type":"string","value":"Medicaión","raw":"\"Medicaión\""},"extraCss":{"type":"string","value":"https://www.google.com,https://www.google.com","raw":"\"https://www.google.com,https://www.google.com\""},"extraHead":{"type":"string","value":"<meta http-equiv=\"Content-Language\"content=\"es-ve\"/>","raw":"\"<meta http-equiv=\\\"Content-Language\\\"content=\\\"es-ve\\\"/>\""}}` |
| `medicament`          | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `service`             | `object`  |             | `{"id":{"type":"string","value":"","raw":"\"\""},"scheduled_date_time":{"type":"string","value":"","raw":"\"\""},"service_type":{"type":"string","value":"","raw":"\"\""}}`                                                                                                                                                                                                                                                                                                          |
| `dialog`              | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `deliveryMedicaments` | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `headers`             | `array`   |             | `[ { text: this.$t('quantity'), sortable: false, value: 'deliveryQuantity' }, { text: this.$t('unit'), sortable: false, value: 'deliveryUnit' }, { text: this.$t('description'), sortable: false, value: 'medicament' }, { text: this.$t('delivered'), sortable: false, value: 'delivered' } ]`                                                                                                                                                                                      |

## Computed Properties

| Name           | Description                        |
| -------------- | ---------------------------------- |
| `permissions`  | **Dependencies:** `roles`          |
| `fullNameUser` | **Dependencies:** `serviceDetails` |

## Methods

### dateFormat()

**Syntax**

```typescript
dateFormat(date: unknow): unknow
```

### getMedicaments()

**Syntax**

```typescript
getMedicaments(): unknow
```

### getDeliveryMedicaments()

**Syntax**

```typescript
getDeliveryMedicaments(): unknow
```

### printMedicationFromButton()

**Syntax**

```typescript
printMedicationFromButton(): void
```

### getSource()

**Syntax**

```typescript
getSource(sourceId: unknow): unknow
```

### getInsuranceHolderName()

**Syntax**

```typescript
getInsuranceHolderName(policyId: unknow): unknow
```

### getInsuranceHolderDni()

**Syntax**

```typescript
getInsuranceHolderDni(policyId: unknow): unknow
```

### getDiagnoses()

**Syntax**

```typescript
getDiagnoses(): unknow
```

### getPhones()

**Syntax**

```typescript
getPhones(): unknow
```

