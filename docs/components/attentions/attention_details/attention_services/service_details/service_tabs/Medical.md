# Medical

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Medical>` se utiliza para mostrar y editar información
la médica relacionada con sintomas y diagnosticos del servicio contenido
en una atención.

`<Medical>` es hijo del componente `<ServiceDetails>`.

---

The `<Medical>` component is used to display and edit medical
information related to symptoms and diagnosis of the service contained
in an attention.

`<Medical>` is a child of the `<ServiceDetails>` component.

## Data

| Name                         | Type      | Description | Initial value                                                                      |
| ---------------------------- | --------- | ----------- | ---------------------------------------------------------------------------------- |
| `snackbar`                   | `boolean` |             | `false`                                                                            |
| `snackbarAlert`              | `boolean` |             | `false`                                                                            |
| `text`                       | `string`  |             | `""`                                                                               |
| `textAlarm`                  | `string`  |             | `"Evaluación urgente. Se sugiere AMD o Traslado."`                                 |
| `timeout`                    | `number`  |             | `6000`                                                                             |
| `color`                      | `string`  |             | `""`                                                                               |
| `isEditing`                  | `boolean` |             | `false`                                                                            |
| `symptoms`                   | `array`   |             | `[]`                                                                               |
| `other_symptoms`             | `string`  |             | `""`                                                                               |
| `diagnoses`                  | `array`   |             | `[]`                                                                               |
| `other_diagnoses`            | `string`  |             | `""`                                                                               |
| `symptomsResults`            | `array`   |             | `[]`                                                                               |
| `diagnosesResults`           | `array`   |             | `[]`                                                                               |
| `emergencyAlert`             | `boolean` |             | `false`                                                                            |
| `recommendations`            | `string`  |             | `""`                                                                               |
| `alarmSigns`                 | `string`  |             | `""`                                                                               |
| `mainCallReason`             | `string`  |             | `""`                                                                               |
| `mainCallReasonItems`        | `array`   |             | `[]`                                                                               |
| `selectedMandatoryQuestions` | `array`   |             | `[]`                                                                               |
| `selectedOptionalQuestions`  | `array`   |             | `[]`                                                                               |
| `headers`                    | `array`   |             | `[ { text: 'Pregunta', align: 'left', sortable: false, value: 'question_text' } ]` |
| `mandatoryQuestionItems`     | `array`   |             | `[]`                                                                               |
| `optionalQuestionItems`      | `array`   |             | `[]`                                                                               |

## Computed Properties

| Name                                  | Description                                                                                                                 |
| ------------------------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| `permissions_practitioner`            | **Dependencies:** `roles`                                                                                                   |
| `permissions_dispatcher`              | **Dependencies:** `roles`                                                                                                   |
| `permissions_dispatcher_operador`     | **Dependencies:** `roles`                                                                                                   |
| `permissions_reason_selection`        | **Dependencies:** `roles`                                                                                                   |
| `permissions_medical_details_edition` | **Dependencies:** `permissions_practitioner`, `serviceDetails`, `permissions_dispatcher`, `permissions_dispatcher_operador` |
| `isDisabled`                          | **Dependencies:** `isEditing`, `symptoms`, `other_symptoms`, `diagnoses`, `other_diagnoses`                                 |

## Methods

### fullTerm()

**Syntax**

```typescript
fullTerm(item: unknow): unknow
```

### fullMainCallReasonTerm()

**Syntax**

```typescript
fullMainCallReasonTerm(item: unknow): unknow
```

### getSymptoms()

**Syntax**

```typescript
getSymptoms(): unknow
```

### getDiagnoses()

**Syntax**

```typescript
getDiagnoses(): unknow
```

### getMainCallReasonItems()

**Syntax**

```typescript
getMainCallReasonItems(): unknow
```

### getMainCallReason()

**Syntax**

```typescript
getMainCallReason(): unknow
```

### getSelectedMandatoryQuestions()

**Syntax**

```typescript
getSelectedMandatoryQuestions(): unknow
```

### getSelectedOptionalQuestions()

**Syntax**

```typescript
getSelectedOptionalQuestions(): unknow
```

### getMessages()

**Syntax**

```typescript
getMessages(): void
```

### getQuestions()

**Syntax**

```typescript
getQuestions(): void
```

### updateMainCallReasonItems()

**Syntax**

```typescript
updateMainCallReasonItems(): void
```

### save()

**Syntax**

```typescript
save(): void
```

