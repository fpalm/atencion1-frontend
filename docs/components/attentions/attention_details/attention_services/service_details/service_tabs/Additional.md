# Additional

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Additional>` no esta usado en el sistema, a desarrollarse a
futuro

---

The `<Additional>` component is not used in the system, to be developed in the
future

## Data

| Name           | Type      | Description | Initial value |
| -------------- | --------- | ----------- | ------------- |
| `hasSaved`     | `boolean` |             | `false`       |
| `isEditing`    | `any`     |             | `null`        |
| `model`        | `any`     |             | `null`        |
| `origin`       | `string`  |             | `""`          |
| `destination`  | `string`  |             | `""`          |
| `travelReturn` | `string`  |             | `""`          |
| `kmTraveled`   | `string`  |             | `""`          |
| `request`      | `string`  |             | `""`          |
| `control`      | `string`  |             | `""`          |
| `receipt`      | `string`  |             | `""`          |

## Computed Properties

| Name          | Description               |
| ------------- | ------------------------- |
| `permissions` | **Dependencies:** `roles` |

## Methods

### save()

**Syntax**

```typescript
save(): void
```

