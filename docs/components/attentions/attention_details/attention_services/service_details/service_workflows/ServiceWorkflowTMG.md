# ServiceWorkflowTMG

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<ServiceWorkflowTMG>` se utiliza para monitorear en cada etapa
el flujo de trabajo dentro de los servicios OMT (TMG por su siglas en inglés).

`<ServiceWorkflowTMG>` es hijo del componente `<ServiceDetails>`.

---

The `<ServiceWorkflowTMG>` is used to monitor at each stage
the workflow within TMG services

`<ServiceWorkflowTMG>` is a child of the `<ServiceDetails>` component.

## Data

| Name                 | Type      | Description | Initial value |
| -------------------- | --------- | ----------- | ------------- |
| `currentStep`        | `any`     |             | `null`        |
| `callStartDatetime`  | `string`  |             | `""`          |
| `callEndDatetime`    | `string`  |             | `""`          |
| `isEditing`          | `boolean` |             | `false`       |
| `editWorkflow`       | `boolean` |             | `false`       |
| `callStartDate`      | `string`  |             | `""`          |
| `callStartTime`      | `string`  |             | `""`          |
| `callEndDate`        | `string`  |             | `""`          |
| `callEndTime`        | `string`  |             | `""`          |
| `snackbar`           | `boolean` |             | `false`       |
| `text`               | `string`  |             | `""`          |
| `timeout`            | `number`  |             | `6000`        |
| `color`              | `string`  |             | `""`          |
| `showProgressDialog` | `boolean` |             | `false`       |

## Computed Properties

| Name                                 | Description                                                                      |
| ------------------------------------ | -------------------------------------------------------------------------------- |
| `permissions`                        | **Dependencies:** `roles`                                                        |
| `isDisabled`                         | **Dependencies:** `callStartDate`, `callStartTime`, `callEndDate`, `callEndTime` |
| `tmgCompleteServiceButtonIsDisabled` | **Dependencies:** `serviceDetails`                                               |

## Methods

### getIconsColor()

**Syntax**

```typescript
getIconsColor(step: unknow): void
```

### getCurrentDatetime()

**Syntax**

```typescript
getCurrentDatetime(): unknow
```

### setCallStartDatetime()

**Syntax**

```typescript
setCallStartDatetime(): void
```

### setCallEndDatetime()

**Syntax**

```typescript
setCallEndDatetime(): void
```

### formatDatetime()

**Syntax**

```typescript
formatDatetime(datetime: unknow): unknow
```

### getCurrentStep()

**Syntax**

```typescript
getCurrentStep(): void
```

### setDatetimes()

**Syntax**

```typescript
setDatetimes(): void
```

### assignDatetimes()

**Syntax**

```typescript
assignDatetimes(): void
```

### saveDatetimes()

**Syntax**

```typescript
saveDatetimes(): void
```

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

