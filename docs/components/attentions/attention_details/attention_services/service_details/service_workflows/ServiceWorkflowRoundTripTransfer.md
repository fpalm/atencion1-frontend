# ServiceWorkflowRoundTripTransfer

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<ServiceWorkflowRoundTripTransfer>` se utiliza para monitorear en
cada etapa
el flujo de trabajo dentro de los servicios TLD de ida y vuelta (Transfer por su
nombre en inglés).

`<ServiceWorkflowRoundTripTransfer>` es hijo del componente `<ServiceDetails>`.

---

The `<ServiceWorkflowRoundTripTransfer>` is used to monitor at each stage
the workflow within TLD round trip services

`<ServiceWorkflowRoundTripTransfer>` is a child of the `<ServiceDetails>`
component.

## Data

| Name                            | Type      | Description | Initial value |
| ------------------------------- | --------- | ----------- | ------------- |
| `currentStep`                   | `number`  |             | `1`           |
| `dispatchDatetime`              | `string`  |             | `""`          |
| `baseDepartureDatetime`         | `string`  |             | `""`          |
| `originArrivalDatetime`         | `string`  |             | `""`          |
| `originDepartureDatetime`       | `string`  |             | `""`          |
| `destinationArrivalDatetime`    | `string`  |             | `""`          |
| `destinationDepartureDatetime`  | `string`  |             | `""`          |
| `originSecondArrivalDatetime`   | `string`  |             | `""`          |
| `originSecondDepartureDatetime` | `string`  |             | `""`          |
| `baseArrivalDatetime`           | `string`  |             | `""`          |
| `isEditing`                     | `boolean` |             | `false`       |
| `editWorkflow`                  | `boolean` |             | `false`       |
| `dispatchDate`                  | `string`  |             | `""`          |
| `dispatchTime`                  | `string`  |             | `""`          |
| `baseDepartureDate`             | `string`  |             | `""`          |
| `baseDepartureTime`             | `string`  |             | `""`          |
| `originArrivalDate`             | `string`  |             | `""`          |
| `originArrivalTime`             | `string`  |             | `""`          |
| `originDepartureDate`           | `string`  |             | `""`          |
| `originDepartureTime`           | `string`  |             | `""`          |
| `destinationArrivalDate`        | `string`  |             | `""`          |
| `destinationArrivalTime`        | `string`  |             | `""`          |
| `destinationDepartureDate`      | `string`  |             | `""`          |
| `destinationDepartureTime`      | `string`  |             | `""`          |
| `originSecondArrivalDate`       | `string`  |             | `""`          |
| `originSecondArrivalTime`       | `string`  |             | `""`          |
| `originSecondDepartureDate`     | `string`  |             | `""`          |
| `originSecondDepartureTime`     | `string`  |             | `""`          |
| `baseArrivalDate`               | `string`  |             | `""`          |
| `baseArrivalTime`               | `string`  |             | `""`          |
| `snackbar`                      | `boolean` |             | `false`       |
| `text`                          | `string`  |             | `""`          |
| `timeout`                       | `number`  |             | `6000`        |
| `color`                         | `string`  |             | `""`          |

## Computed Properties

| Name                                 | Description                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `permissions`                        | **Dependencies:** `roles`                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `isDisabled`                         | **Dependencies:** `dispatchDate`, `dispatchTime`, `baseDepartureDate`, `baseDepartureTime`, `originArrivalDate`, `originArrivalTime`, `originDepartureDate`, `originDepartureTime`, `destinationArrivalDate`, `destinationArrivalTime`, `destinationDepartureDate`, `destinationDepartureTime`, `originSecondArrivalDate`, `originSecondArrivalTime`, `originSecondDepartureDate`, `originSecondDepartureTime`, `baseArrivalDate`, `baseArrivalTime` |
| `stepCompletedButtonDisabled`        | **Dependencies:** `serviceDetails`                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `stepCompletedButtonTooltipDisabled` | **Dependencies:** `serviceDetails`                                                                                                                                                                                                                                                                                                                                                                                                                   |

## Methods

### getIconsColor()

**Syntax**

```typescript
getIconsColor(step: unknow): void
```

### getCurrentDatetime()

**Syntax**

```typescript
getCurrentDatetime(): unknow
```

### setDispatchDatetime()

**Syntax**

```typescript
setDispatchDatetime(): void
```

### setBaseDepartureDatetime()

**Syntax**

```typescript
setBaseDepartureDatetime(): void
```

### setOriginArrivalDatetime()

**Syntax**

```typescript
setOriginArrivalDatetime(): void
```

### setOriginDepartureDatetime()

**Syntax**

```typescript
setOriginDepartureDatetime(): void
```

### setDestinationArrivalDatetime()

**Syntax**

```typescript
setDestinationArrivalDatetime(): void
```

### setDestinationDepartureDatetime()

**Syntax**

```typescript
setDestinationDepartureDatetime(): void
```

### setOriginSecondArrivalDatetime()

**Syntax**

```typescript
setOriginSecondArrivalDatetime(): void
```

### setOriginSecondDepartureDatetime()

**Syntax**

```typescript
setOriginSecondDepartureDatetime(): void
```

### setBaseArrivalDatetime()

**Syntax**

```typescript
setBaseArrivalDatetime(): void
```

### formatDatetime()

**Syntax**

```typescript
formatDatetime(datetime: unknow): unknow
```

### getCurrentStep()

**Syntax**

```typescript
getCurrentStep(): void
```

### setDatetimes()

**Syntax**

```typescript
setDatetimes(): void
```

### assignDatetimes()

**Syntax**

```typescript
assignDatetimes(): void
```

### saveDatetimes()

**Syntax**

```typescript
saveDatetimes(): void
```

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

