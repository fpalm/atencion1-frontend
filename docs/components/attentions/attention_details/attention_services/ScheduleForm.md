# ScheduleForm

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<ScheduleForm>` se utiliza generar o modificar las fechas
de programación de un servicio.

Se accede a `<ScheduleForm>` desde el componente `<General>` y
de la carpeta attention > attention_details > attention_services >
service_details > service_tabs.

---

The component `<ScheduleForm>` is used to generate or modify dates
of a scheduled service.

The `<ScheduleForm>` is accessed from the component `<General>` from folder
attention > attention_details > attention_services > service_details >
service_tabs.

## Props

| Name               | Type      | Description |
| ------------------ | --------- | ----------- |
| `visible`          | `Boolean` |             |
| `default-datetime` | `String`  | &nbsp;      |

## Data

| Name      | Type             | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| --------- | ---------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `service` | `object`         |             | `{"date":{"type":"ConditionalExpression","value":"moment(new Date(this.defaultDatetime).toISOString()).isBefore(moment(new Date().toISOString()))\n          ? ''\n          : new Date(Date.UTC(new Date(this.defaultDatetime).getFullYear(),\n            new Date(this.defaultDatetime).getMonth(), new Date(this.defaultDatetime).getDate(),\n            new Date(this.defaultDatetime).getHours(), new Date(this.defaultDatetime).getMinutes(),\n            new Date(this.defaultDatetime).getSeconds())).toISOString().substr(0, 10)","raw":"moment(new Date(this.defaultDatetime).toISOString()).isBefore(moment(new Date().toISOString()))\n          ? ''\n          : new Date(Date.UTC(new Date(this.defaultDatetime).getFullYear(),\n            new Date(this.defaultDatetime).getMonth(), new Date(this.defaultDatetime).getDate(),\n            new Date(this.defaultDatetime).getHours(), new Date(this.defaultDatetime).getMinutes(),\n            new Date(this.defaultDatetime).getSeconds())).toISOString().substr(0, 10)"},"time":{"type":"ConditionalExpression","value":"moment(new Date(this.defaultDatetime).toISOString()).isBefore(moment(new Date().toISOString()))\n          ? ''\n          : String(new Date(this.defaultDatetime).getHours()).padStart(2, '0') + ':' +\n            String(new Date(this.defaultDatetime).getMinutes()).padStart(2, '0')","raw":"moment(new Date(this.defaultDatetime).toISOString()).isBefore(moment(new Date().toISOString()))\n          ? ''\n          : String(new Date(this.defaultDatetime).getHours()).padStart(2, '0') + ':' +\n            String(new Date(this.defaultDatetime).getMinutes()).padStart(2, '0')"}}` |
| `today`   | `CallExpression` |             | `new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), new Date().getHours(), new Date().getMinutes(), new Date().getSeconds())).toISOString().substr(0, 10)`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

## Computed Properties

| Name                            | Description                          |
| ------------------------------- | ------------------------------------ |
| `show`                          | **Dependencies:** `visible`          |
| `isDisabled`                    | **Dependencies:** `service`          |
| `computedDateFormattedMomentjs` | **Dependencies:** `$i18n`, `service` |

## Events

| Name                | Description                                                          |
| ------------------- | -------------------------------------------------------------------- |
| `close`             |                                                                      |
| `datetimeScheduled` | <br>**Arguments**<br><ul><li>**`programmedDatetime: any`**</li></ul> |

## Methods

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

### assignDatetime()

**Syntax**

```typescript
assignDatetime(): void
```

