# DatetimesTable

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<DatetimesTable>` se utiliza para agregar fecha o conjunto
de fechas para programar servicios PHD.

`<DatetimesTable>` es hijo de `<NewService>` sólo para los servicios PHD.

---

The `<DatetimesTable>` component is used to add date or set
of dates to schedule HHP services.

`<DatetimesTable>` is a child of `<NewService>` for HHP services only.

## Props

| Name               | Type      | Description |
| ------------------ | --------- | ----------- |
| `delete-datetimes` | `Boolean` | &nbsp;      |

## Data

| Name                        | Type      | Description | Initial value                                                                                                                                                                                 |
| --------------------------- | --------- | ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `dialog`                    | `boolean` |             | `false`                                                                                                                                                                                       |
| `confirmDeletingItemDialog` | `boolean` |             | `false`                                                                                                                                                                                       |
| `editedIndex`               | `number`  |             | `-1`                                                                                                                                                                                          |
| `editedItem`                | `object`  |             | `{"dates":{"type":"array","value":"[]","raw":"[]"},"time":{"type":"any","value":null,"raw":"null"}}`                                                                                          |
| `deletedIndex`              | `number`  |             | `-1`                                                                                                                                                                                          |
| `deletedItem`               | `object`  |             | `{"dates":{"type":"array","value":"[]","raw":"[]"},"time":{"type":"any","value":null,"raw":"null"}}`                                                                                          |
| `headers`                   | `array`   |             | `[ { text: this.$t('dates'), sortable: false, value: 'dates' }, { text: this.$t('time'), sortable: false, value: 'time' }, { text: this.$t('actions'), sortable: false, value: 'actions' } ]` |
| `datetimes`                 | `array`   |             | `[]`                                                                                                                                                                                          |
| `menu`                      | `boolean` |             | `false`                                                                                                                                                                                       |
| `auxDates`                  | `array`   |             | `[]`                                                                                                                                                                                          |

## Computed Properties

| Name                            | Description                                 |
| ------------------------------- | ------------------------------------------- |
| `formTitle`                     | **Dependencies:** `editedIndex`, `$t`       |
| `saveDatetimesButtonText`       | **Dependencies:** `editedIndex`, `$t`       |
| `isDisabled`                    | **Dependencies:** `auxDates`, `editedItem`  |
| `computedDateFormattedMomentjs` | **Dependencies:** `formatDates`, `auxDates` |

## Events

| Name              | Description                                                 |
| ----------------- | ----------------------------------------------------------- |
| `updateDatetimes` | <br>**Arguments**<br><ul><li>**`datetimes: any`**</li></ul> |

## Methods

### editItem()

**Syntax**

```typescript
editItem(item: unknow): void
```

### confirmDeletingItem()

**Syntax**

```typescript
confirmDeletingItem(item: unknow): void
```

### deleteItem()

**Syntax**

```typescript
deleteItem(): void
```

### close()

**Syntax**

```typescript
close(): void
```

### saveDatetimes()

**Syntax**

```typescript
saveDatetimes(): void
```

### resetValidations()

**Syntax**

```typescript
resetValidations(): void
```

### formatDates()

**Syntax**

```typescript
formatDates(dates: unknow): void
```

### displayDates()

**Syntax**

```typescript
displayDates(dates: unknow): unknow
```

