# AttachedDocuments

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<AttachedDocuments>` se utiliza para cargar documentos a la
aplicación.

Se accede a `<AttachedDocuments>` desde los componentes`<AttentionDetails>`,
`<ServiceDetails>` y `<AttentionDetailsDialog>`.

---

The `<Policy>` component is used to upload documents to the application.

You can access `<AttachedDocuments>` from the components: `<AttentionDetails>`,
`<ServiceDetails>` and `<AttentionDetailsDialog>`

## Props

| Name             | Type     | Description |
| ---------------- | -------- | ----------- |
| `activator-type` | `Number` | &nbsp;      |

## Data

| Name                        | Type      | Description | Initial value                                                                                                                                                                                                                                                                  |
| --------------------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `loading`                   | `boolean` |             | `false`                                                                                                                                                                                                                                                                        |
| `dialog`                    | `boolean` |             | `false`                                                                                                                                                                                                                                                                        |
| `data`                      | `array`   |             | `[]`                                                                                                                                                                                                                                                                           |
| `headers_open`              | `array`   |             | `[ {text: this.$t('document'), sortable: false, value: 'document'}, {text: this.$t('attached'), sortable: true, value: 'uploaded_at'}, {text: this.$t('description'), sortable: false, value: 'description'}, {text: this.$t('actions'), sortable: false, value: 'actions'} ]` |
| `headers_closed`            | `array`   |             | `[ {text: this.$t('document'), sortable: false, value: 'document'}, {text: this.$t('attached'), sortable: true, value: 'uploaded_at'}, {text: this.$t('description'), sortable: false, value: 'description'} ]`                                                                |
| `type`                      | `string`  |             | `""`                                                                                                                                                                                                                                                                           |
| `description`               | `string`  |             | `""`                                                                                                                                                                                                                                                                           |
| `documentInput`             | `array`   |             | `[]`                                                                                                                                                                                                                                                                           |
| `selectedDocument`          | `any`     |             | `null`                                                                                                                                                                                                                                                                         |
| `snackbar`                  | `boolean` |             | `false`                                                                                                                                                                                                                                                                        |
| `text`                      | `string`  |             | `""`                                                                                                                                                                                                                                                                           |
| `timeout`                   | `number`  |             | `6000`                                                                                                                                                                                                                                                                         |
| `color`                     | `string`  |             | `""`                                                                                                                                                                                                                                                                           |
| `confirmDeletingItemDialog` | `boolean` |             | `false`                                                                                                                                                                                                                                                                        |
| `deletedItem`               | `object`  |             | `{"id":{"type":"any","value":null,"raw":"null"},"document_type":{"type":"any","value":null,"raw":"null"}}`                                                                                                                                                                     |

## Computed Properties

| Name          | Description                                                            |
| ------------- | ---------------------------------------------------------------------- |
| `isDisabled`  | **Dependencies:** `type`, `selectedDocument`                           |
| `doc_headers` | **Dependencies:** `attentionDetails`, `headers_closed`, `headers_open` |

## Methods

### formatCreatedDate()

**Syntax**

```typescript
formatCreatedDate(date: unknow): unknow
```

### getDocumentType()

**Syntax**

```typescript
getDocumentType(id: unknow): unknow
```

### getMedDocuments()

**Syntax**

```typescript
getMedDocuments(): void
```

### getHHCLabDocuments()

**Syntax**

```typescript
getHHCLabDocuments(): void
```

### getLabDocuments()

**Syntax**

```typescript
getLabDocuments(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

### close()

**Syntax**

```typescript
close(): void
```

### onDocumentSelected()

**Syntax**

```typescript
onDocumentSelected(file: unknow): void
```

### attachDocument()

**Syntax**

```typescript
attachDocument(): void
```

### openLink()

**Syntax**

```typescript
openLink(url: unknow): void
```

### confirmDeletingItem()

**Syntax**

```typescript
confirmDeletingItem(item: unknow): void
```

### deleteItem()

**Syntax**

```typescript
deleteItem(): void
```

