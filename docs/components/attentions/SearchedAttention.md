# SearchedAttention

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<SearchedAttention>` es el campo de búsqueda de
atenciones por numero de atención.

`<SearchedAttention>` es hijo del componente `<Attentions>`.

---

The component `<SearchedAttention>` is the search field of
attentions by number of attention.

`<SearchedAttention>` is a child of the `<Attentions>` component.

## Data

| Name                | Type     | Description | Initial value |
| ------------------- | -------- | ----------- | ------------- |
| `searchTerm`        | `string` |             | `""`          |
| `found`             | `any`    |             | `null`        |
| `showErrorAlert`    | `any`    |             | `null`        |
| `showNotFoundAlert` | `any`    |             | `null`        |

## Methods

### attentionSearch()

**Syntax**

```typescript
attentionSearch(term: unknow): void
```

