# NotAffiliate

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<NotAffiliate>` se utiliza para presentar un formulario de
creación de un nuevo afiliado. Además, muestra los resultados de la búsqueda
del afiliado en el `Validador`, mediante el componente `<Validator>`.

`<NotAffiliate>` hace uso del componente `<AddressForm>` para la asignación
de la dirección personal del afiliado. Mediante el componente
`<NewOrganization>`,
se podrá añadir una nueva organización en el listado del campo `Contratante`.

`<NotAffiliate>` es hijo del componente `<NewAttention>`.

---

The `<NotAffiliate>` component is used to display the new affiliate creation
form. In addition, it shows the results of the affiliate in the
`Validator`, by using the` <Validator> `component.

`<NotAffiliate>` makes use of the `<AddressForm>` component to assign the
affiliate's personal address. Through the `<NewOrganization>` component, a
new organization can be added in the list of the `Contractor` field.

`<NotAffiliate>` is a child of the `<NewAttention>` component.

## Props

| Name          | Type     | Description |
| ------------- | -------- | ----------- |
| `search-term` | `String` | &nbsp;      |

## Data

| Name                   | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------------- | --------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `filter`               | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `plan`                 | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `sponsor`              | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `client`               | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `activator`            | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `attach`               | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `editing`              | `any`     |             | `null`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `items`                | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `searchPhone`          | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `searchEmail`          | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `affiliate`            | `object`  |             | `{"policies":{"type":"array","value":"[]","raw":"[]"},"first_name":{"type":"string","value":"","raw":"\"\""},"last_name":{"type":"string","value":"","raw":"\"\""},"gender":{"type":"string","value":"","raw":"\"\""},"emails":{"type":"array","value":"[]","raw":"[]"},"birthdate":{"type":"CallExpression","value":"new Date(1990, 0, 1).toISOString().substr(0, 10)","raw":"new Date(1990, 0, 1).toISOString().substr(0, 10)"},"affiliate_addresses":{"type":"array","value":"[]","raw":"[]"},"phone_numbers":{"type":"array","value":"[]","raw":"[]"},"dni":{"type":"string","value":"","raw":"\"\""},"dni_type":{"type":"string","value":"V","raw":"\"V\""},"from_validator":{"type":"string","value":"","raw":"\"\""},"authorized_by":{"type":"string","value":"","raw":"\"\""},"registered_by":{"type":"string","value":"","raw":"\"\""}}` |
| `contractType`         | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `dialog`               | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `genderItems`          | `array`   |             | `[ {code: 'MALE', display: this.$t('male')}, {code: 'FEMALE', display: this.$t('female')} ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `contractItems`        | `array`   |             | `[ {code: 'INDIVIDUAL', display: this.$t('individual')}, {code: 'GROUP', display: this.$t('collective')} ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `dniTypeItems`         | `array`   |             | `[ 'V', 'E', 'P' ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `phoneNumberChips`     | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `phoneMask`            | `string`  |             | `"0##########"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `dniMask`              | `string`  |             | `"########"`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `emailChips`           | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `snackbar`             | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `text`                 | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `timeout`              | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `color`                | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `address`              | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `addressObject`        | `object`  |             | `{}`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `showAddressForm`      | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `showOrganizationForm` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |

## Computed Properties

| Name                                 | Description                                                                                                  |
| ------------------------------------ | ------------------------------------------------------------------------------------------------------------ |
| `computedStartDateFormattedMomentjs` | **Dependencies:** `$i18n`, `affiliate`                                                                       |
| `getAge`                             | **Dependencies:** `affiliate`                                                                                |
| `isDisabled`                         | **Dependencies:** `formFilter`, `affiliate`, `plan`, `phoneNumberChips`, `getAge`, `contractType`, `sponsor` |
| `filteredPlans`                      | **Dependencies:** `planItems`, `client`                                                                      |
| `formFilter`                         | **Dependencies:** `organizationWithPlanItems`, `client`                                                      |

## Methods

### getPlan()

**Syntax**

```typescript
getPlan(pickedPlan: unknow): unknow
```

### updatePhoneTags()

**Syntax**

```typescript
updatePhoneTags(): void
```

### updateEmailTags()

**Syntax**

```typescript
updateEmailTags(): void
```

### alert()

**Syntax**

```typescript
alert(): void
```

### createAffiliate()

**Syntax**

```typescript
createAffiliate(): void
```

### removePhoneNumber()

**Syntax**

```typescript
removePhoneNumber(item: unknow): void
```

### removeEmail()

**Syntax**

```typescript
removeEmail(item: unknow): void
```

### resetForm()

**Syntax**

```typescript
resetForm(): void
```

### onAddressAssigned()

**Syntax**

```typescript
onAddressAssigned(...args: unknow[]): void
```

### fullNameUser()

**Syntax**

```typescript
fullNameUser(item: unknow): unknow
```

### onOrganizationCreated()

**Syntax**

```typescript
onOrganizationCreated(value: unknow): void
```

### deletePlan()

**Syntax**

```typescript
deletePlan(): void
```

