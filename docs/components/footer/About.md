# About

**Author:** Ixhel Mejías

El componente `<About>` se utiliza para mostrar información importante sobre
la aplicación `Atención-1`, como descripción, versión y tecnologías.

---

The `<About>` component is used to display important information about the
application `Atención-1`, such as description, version and technologies.

## Props

| Name      | Type      | Description                                       |
| --------- | --------- | ------------------------------------------------- |
| `visible` | `Boolean` | Controls whether the dialog is visible or hidden. |

## Data

| Name           | Type    | Description                                                                              | Initial value                                                                                                                                                                                      |
| -------------- | ------- | ---------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `technologies` | `array` | The technologies used in the development of the application, to be listed in the dialog. | `[ { id: 1, text: 'Django v2.1.1' }, { id: 2, text: 'Django REST Framework v3.10.3' }, { id: 3, text: 'PostgreSQL v.11.2' }, { id: 4, text: 'Vue v.2.5.2' }, { id: 5, text: 'Node.js v.6.0.0' } ]` |

## Computed Properties

| Name   | Description                                                                                                         |
| ------ | ------------------------------------------------------------------------------------------------------------------- |
| `show` | The `show` computed property uses a getter and a setter to manage dialog visibility.<br>**Dependencies:** `visible` |

## Events

| Name    | Description                                                                                                                                        |
| ------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| `close` | When the dialog's close button is clicked, it triggers the `set` function which emits a `close` event to the parent component to close the dialog. |

