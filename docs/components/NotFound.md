# NotFound

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<NotFound>` se usa como enlace que conduce al
"dead link" (enlace muerto).

Surge en el momento que se escriba una dirección url que no esté construida.

---

The `<NotFound>` component is used as a link that leads to
"dead link".

It arises at the time of writing a url address that is not built.

## Data

| Name   | Type             | Description | Initial value          |
| ------ | ---------------- | ----------- | ---------------------- |
| `text` | `CallExpression` |             | `this.$t('not_found')` |

