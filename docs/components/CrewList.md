# CrewList

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<CrewList>` es usado para desplegar el listado de
tripulaciones que estan disponibles para ser asignados a servicios.

Representa la página que aparece an seleccionar Tripulaciones en el menú
principal.

---

The `<CrewList>` component is used to display the
crews that are available for assignment to services.

Represents the page that appears when you select Crew from the menu
main.

## Data

| Name                  | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| --------------------- | --------- | ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `printObj`            | `object`  |             | `{"id":{"type":"string","value":"printCrew","raw":"\"printCrew\""},"popTitle":{"type":"string","value":"Solicitudes","raw":"\"Solicitudes\""},"extraCss":{"type":"string","value":"https://www.google.com,https://www.google.com","raw":"\"https://www.google.com,https://www.google.com\""},"extraHead":{"type":"string","value":"<meta http-equiv=\"Content-Language\"content=\"es-ve\"/>","raw":"\"<meta http-equiv=\\\"Content-Language\\\"content=\\\"es-ve\\\"/>\""}}`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `snackbar`            | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `text`                | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `timeout`             | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `color`               | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `editedIndex`         | `number`  |             | `-1`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `pagination`          | `object`  |             | `{"totalItems":{"type":"Object","value":"this.$store.getters.crewTodayPages.count","raw":"this.$store.getters.crewTodayPages.count"},"rowsPerPage":{"type":"number","value":10,"raw":"10"},"page":{"type":"number","value":1,"raw":"1"}}`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `search`              | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `loading`             | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `dialog`              | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `editDialog`          | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `deleteDialog`        | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `calendarMenuStart`   | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `calendarMenuEnd`     | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `watchMenuStart`      | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `watchMenuEnd`        | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `titles`              | `array`   |             | `[ { text: this.$t('crews'), disabled: true } ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `editedItem`          | `object`  |             | `{"start_date":{"type":"Object","value":"this.getCurrentDatetime","raw":"this.getCurrentDatetime"},"start_time":{"type":"BinaryExpression","value":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')","raw":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')"},"end_date":{"type":"Object","value":"this.getCurrentDatetime","raw":"this.getCurrentDatetime"},"end_time":{"type":"BinaryExpression","value":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')","raw":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')"},"mobile_unit":{"type":"string","value":"","raw":"\"\""},"base":{"type":"string","value":"","raw":"\"\""},"practitioner":{"type":"string","value":"","raw":"\"\""},"paramedic_crew":{"type":"string","value":"","raw":"\"\""},"paramedic_driver":{"type":"string","value":"","raw":"\"\""}}` |
| `headers`             | `array`   |             | `[ {text: this.$t('crew_name'), sortable: false, value: 'crew_code'}, {text: this.$t('unit'), sortable: false, value: 'unit'}, {text: this.$t('paramedic_driver'), align: 'left', sortable: false, value: 'paramedic_driver'}, {text: this.$t('paramedic_crew'), sortable: false, value: 'paramedic_crew'}, {text: this.$t('doctor'), sortable: false, value: 'practitioner'}, {text: this.$t('actions'), sortable: false, value: 'actions'} ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `headers_permissions` | `array`   |             | `[ {text: this.$t('crew_name'), sortable: false, value: 'crew_code'}, {text: this.$t('unit'), sortable: false, value: 'unit'}, {text: this.$t('paramedic_driver'), align: 'left', sortable: false, value: 'paramedic_driver'}, {text: this.$t('paramedic_crew'), sortable: false, value: 'paramedic_crew'}, {text: this.$t('doctor'), sortable: false, value: 'practitioner'} ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `data`                | `array`   |             | `[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

## Computed Properties

| Name                                 | Description                                                       |
| ------------------------------------ | ----------------------------------------------------------------- |
| `permissions`                        | **Dependencies:** `roles`                                         |
| `getHeaders`                         | **Dependencies:** `permissions`, `headers`, `headers_permissions` |
| `getCurrentDatetime`                 |                                                                   |
| `pages`                              | **Dependencies:** `pagination`                                    |
| `computedStartDateFormattedMomentjs` | **Dependencies:** `$i18n`, `editedItem`                           |
| `computedEndDateFormattedMomentjs`   | **Dependencies:** `$i18n`, `editedItem`                           |

## Methods

### onCrewCreation()

Triggered when event is emitted by the child.

**Syntax**

```typescript
onCrewCreation(): void
```

### fullNamePractitioner()

**Syntax**

```typescript
fullNamePractitioner(item: unknow): unknow
```

### formatDatetime()

**Syntax**

```typescript
formatDatetime(datetime: unknow): unknow
```

### dateTimeFormat()

**Syntax**

```typescript
dateTimeFormat(date: unknow, time: unknow): unknow
```

### showDetails()

**Syntax**

```typescript
showDetails(item: unknow): void
```

### paginationChangeHandler()

**Syntax**

```typescript
paginationChangeHandler(pageNumber: unknow): void
```

### editItem()

**Syntax**

```typescript
editItem(item: unknow): void
```

### deleteItem()

**Syntax**

```typescript
deleteItem(item: unknow): void
```

### deletion()

**Syntax**

```typescript
deletion(): void
```

### close()

**Syntax**

```typescript
close(): void
```

### getPractitioners()

**Syntax**

```typescript
getPractitioners(): unknow
```

### save()

**Syntax**

```typescript
save(): void
```

### getData()

**Syntax**

```typescript
getData(): void
```

### setData()

**Syntax**

```typescript
setData(results: unknow): void
```

### getPractitioner()

**Syntax**

```typescript
getPractitioner(item: unknow, role: unknow): unknow
```

### getDate()

**Syntax**

```typescript
getDate(date: unknow): unknow
```

### getTime()

**Syntax**

```typescript
getTime(time: unknow): unknow
```

