# Attentions

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Attentions>` se utiliza para mostrar los listados de
atenciones y su correspondiente leyenda de color.

Representa la página principal de la aplicación Atención-1. Una vez que ha
iniciado sesión, el usuario es dirigido a esta interfaz de listados de
atenciones.

---

The `<Attentions>` component is used to display the lists of attentions and
their corresponding color legend.

It represents the main page of the application `Atención-1` for a logged in
user. Once logged in, the user is directed to this attentions lists interface.

## Data

| Name     | Type    | Description                                                                           | Initial value                                                                |
| -------- | ------- | ------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| `titles` | `array` | The array of objects passed to the `items` property of the `v-breadcrumbs` component. | `[ { text: this.$t('attentions'), disabled: true, href: '/#/attentions' } ]` |

## Computed Properties

| Name                             | Description               |
| -------------------------------- | ------------------------- |
| `permissions_attention_creation` | **Dependencies:** `roles` |

## Methods

### goToNewAttention()

**Syntax**

```typescript
goToNewAttention(): void
```

### scroll_search()

**Syntax**

```typescript
scroll_search(): void
```

### scroll_onhold()

**Syntax**

```typescript
scroll_onhold(): void
```

### scroll_inverification()

**Syntax**

```typescript
scroll_inverification(): void
```

### scroll_scheduled()

**Syntax**

```typescript
scroll_scheduled(): void
```

### scroll_inprogress()

**Syntax**

```typescript
scroll_inprogress(): void
```

### scroll_completed()

**Syntax**

```typescript
scroll_completed(): void
```

