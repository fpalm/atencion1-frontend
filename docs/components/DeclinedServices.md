# DeclinedServices

## Props

| Name           | Type     | Description |
| -------------- | -------- | ----------- |
| `attention-id` | `String` | &nbsp;      |

## Data

| Name       | Type    | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                              |
| ---------- | ------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `titles`   | `array` |             | `[ { text: this.$t('declined_services'), disabled: true } ]`                                                                                                                                                                                                                                                                                                                               |
| `headers`  | `array` |             | `[ {text: this.$t('code'), align: 'left', sortable: false, value: 'code'}, {text: this.$t('type'), sortable: false, value: 'type.display'}, {text: this.$t('sector'), sortable: false, value: 'sector'}, {text: this.$t('status'), sortable: false, value: 'status'} ]`                                                                                                                    |
| `services` | `array` |             | `[ { code: 1913381, type: {code: 2, display: 'OMT'}, sector: 'Montalban', status: 'Finished', attention: this.attentionId }, { code: 1913382, type: {code: 1, display: 'AMD'}, sector: 'Montalban', status: 'Finished', attention: this.attentionId }, { code: 1913383, type: {code: 3, display: 'Traslado'}, sector: 'Montalban', status: 'In Progress', attention: this.attentionId } ]` |

## Methods

### showDetails()

**Syntax**

```typescript
showDetails(item: unknow): void
```

