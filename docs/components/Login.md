# Login

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Login>` se usa para permitir el ingreso de usuarios a la
aplicación.

Representa la página principal de la aplicación cuando se muestra en el
navegador.

En este componente se hace el llamado de las variables globales usando vuex, con
la finalidad de tener precargada la información necesaria para inciar procesos
dentro de la aplicación.

Al ingresar por login a la aplicación, conduce al componente `<Attentions>`.

---

The `<Login>` component is used to allow users to enter the application.

It represents the main page of the application when it is displayed in the
browser.

In this component the call of the global variables is done using vuex, with
the purpose of having the necessary information preloaded to start processes
within the application.

When you login to the application, it leads to the `<Attentions>` component.

## Data

| Name       | Type      | Description | Initial value |
| ---------- | --------- | ----------- | ------------- |
| `username` | `string`  |             | `""`          |
| `password` | `string`  |             | `""`          |
| `snackbar` | `boolean` |             | `false`       |
| `text`     | `string`  |             | `""`          |
| `timeout`  | `number`  |             | `6000`        |
| `color`    | `string`  |             | `""`          |

## Computed Properties

| Name         | Description                              |
| ------------ | ---------------------------------------- |
| `isDisabled` | **Dependencies:** `username`, `password` |

## Methods

### authenticate()

**Syntax**

```typescript
authenticate(): void
```

