# AffiliatesTable

**Author:** Ixhel Mejías

El componente `<AffiliatesTable>` se utiliza para mostrar un listado de
afiliados resultantes de una búsqueda, en forma tabular. La tabla permite la
paginación, mostrando hasta cinco filas por página.

Cada fila contendrá información relevante sobre el afiliado: nombre, apellido,
documento de identidad, fecha de nacimiento, edad, sexo y estado
(activo/inactivo). Si se expande la fila, el usuario puede visualizar más
información: números telefónicos, correos electrónicos, direcciones, pólizas
e historial de atenciones.

`<AffiliatesTable>` es hijo del componente `<Affiliates>`, y será renderizado
si se encuentra el término buscado.

---

The `<AffiliatesTable>` component is used to display a list of the affiliates
resulting from a search, in a tabular form. The table enables pagination,
showing up to five rows per page.

Each row will contain relevant information about the affiliate: first name,
last name, DNI, birthdate, age, gender, and status (active/inactive). If the
row is expanded, the user can visualize more information: phone numbers,
emails, addresses, policies, and care history.

`<AffiliatesTable>` is child of the `<Affiliates>` component, and will be
rendered if the searched term is found.

## Props

| Name          | Type     | Description                                                                        |
| ------------- | -------- | ---------------------------------------------------------------------------------- |
| `search-term` | `String` | The text that represents the search term (affiliate first name, last name or DNI). |

## Data

| Name           | Type      | Description                                                                                                                                                                                                                                                                                                                                | Initial value                                                                                                                                                                                                                                                                                                                                                                                                |
| -------------- | --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `loading`      | `boolean` | Controls whether the loading status is active or not, passed to the `loading` property of the `v-data-table` component. It is used to indicate that data in the table is currently loading, displaying a progress bar and a message.                                                                                                       | `false`                                                                                                                                                                                                                                                                                                                                                                                                      |
| `pagination`   | `object`  | Indicates total items, rows per page, and current page; used for pagination handling. It is passed to the `options` property of the `v-data-table` component (used with `.sync` modifier).                                                                                                                                                 | `{"totalItems":{"type":"Object","value":"this.$store.getters.affiliatePages.count","raw":"this.$store.getters.affiliatePages.count"},"rowsPerPage":{"type":"number","value":5,"raw":"5"},"page":{"type":"number","value":1,"raw":"1"}}`                                                                                                                                                                      |
| `data`         | `array`   | The array of objects passed to the `items` property of the `v-data-table` component, resulting from the affiliates search query.                                                                                                                                                                                                           | `[]`                                                                                                                                                                                                                                                                                                                                                                                                         |
| `headers`      | `array`   | The array of objects that each describe a header column, passed to the `headers` property of the `v-data-table` component.                                                                                                                                                                                                                 | `[ {text: this.$t('first_name'), value: 'first_name'}, {text: this.$t('last_name'), value: 'last_name'}, {text: this.$t('id'), value: 'dni'}, {text: this.$t('birthdate'), value: 'birthdate'}, {text: this.$t('age') + ' (' + this.$t('years') + ')', value: 'age'}, {text: this.$t('gender'), value: 'gender'}, {text: this.$t('active'), value: 'is_active'}, { text: '', value: 'data-table-expand' } ]` |
| `expanded`     | `array`   | The array of expanded items, passed to the `expanded` property of the `v-data-table` component (used with `.sync` modifier).<br>This data property is watched in order to obtain complete information (including updated care history) about the expanded item's corresponding affiliate, by dispatching the `getAffiliateDetails` action. | `[]`                                                                                                                                                                                                                                                                                                                                                                                                         |
| `singleExpand` | `boolean` | Indicates the table's expansion mode, passed to the `single-expand` property of the `v-data-table` component.                                                                                                                                                                                                                              | `true`                                                                                                                                                                                                                                                                                                                                                                                                       |

## Computed Properties

| Name    | Description                                                                                                                                                                                                     |
| ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `pages` | The `pages` computed property calculates the number of pages to display, based on the total items (`pagination.totalItems`) and the rows per page (`pagination.rowsPerPage`).<br>**Dependencies:** `pagination` |

## Methods

### dateFormattedMomentjs()

The `dateFormattedMomentjs` method assigns the 'MMMM Do YYYY' format to
the given date with the external library Moment.js.

It is used for formatting the affiliate's birthdate.

**Syntax**

```typescript
dateFormattedMomentjs(date: string): string
```

**Parameters**

- `date: string`<br>
  Date to format

**Return value**

Formatted date

### paginationChangeHandler()

The `paginationChangeHandler` method assigns the new page number to
`pagination.page` and then calls the method `getData` to update the table's
items.

**Syntax**

```typescript
paginationChangeHandler(pageNumber: number): void
```

**Parameters**

- `pageNumber: number`<br>
  New page number

### getData()

The `getData` method executes a query search for affiliates and then
calls the `setData` method to save the results.

This method is called when the `created` lifecycle hook is run and on
page change (through the `paginationChangeHandler` method).

**Syntax**

```typescript
getData(): void
```

### setData()

The `setData` method assigns the query results to the data property `data`.

**Syntax**

```typescript
setData(results: array): void
```

**Parameters**

- `results: array`<br>
  Query results

### getAge()

The `getAge` method calculates the affiliate's age based on the given
birthdate, by using the function `diff` from the external library
Moment.js.

**Syntax**

```typescript
getAge(birthdate: string): number
```

**Parameters**

- `birthdate: string`<br>
  Affiliate's birthdate

**Return value**

Affiliate's age (years)

