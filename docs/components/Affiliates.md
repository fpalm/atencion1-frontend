# Affiliates

**Author:** Ixhel Mejías

El componente `<Affiliates>` permite buscar afiliados a través de un cuadro
de búsqueda y visualizar los resultados. La búsqueda se puede realizar por
nombre, apellido o documento de identidad del afiliado.

Es padre del componente `<AffiliatesTable>`, el cual se renderiza si se
encuentra el término buscado.

Si el término buscado no es válido o no se encuentra, se muestran mensajes de
alerta de error o advertencia, respectivamente.

---

The `<Affiliates>` component enables searching for affiliates through a
search box, and visualizing the results. The search can be done by the
affiliate's first name, last name or DNI.

It is parent of the `<AffiliatesTable>` component, which is rendered if the
searched term is found.

If the searched term is invalid or not found, error or warning alert messages
are shown, respectively.

## Data

| Name                | Type      | Description                                                                           | Initial value                                                                |
| ------------------- | --------- | ------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| `titles`            | `array`   | The array of objects passed to the `items` property of the `v-breadcrumbs` component. | `[ { text: this.$t('affiliates'), disabled: true, href: '/#/affiliates' } ]` |
| `searchTerm`        | `string`  | The text that represents the search term (affiliate first name, last name or DNI).    | `""`                                                                         |
| `found`             | `boolean` | Indicates if the search term is found.                                                | `false`                                                                      |
| `showErrorAlert`    | `boolean` | Controls whether the invalid term error alert is shown or not.                        | `false`                                                                      |
| `showNotFoundAlert` | `boolean` | Controls whether the term not found warning alert is shown or not.                    | `false`                                                                      |

## Methods

### affiliateSearch()

The `affiliateSearch` method searches for the term indicated by the
user. If the search term is an empty string, the invalid term alert
message is displayed. Otherwise, the `getAffiliateItems` action is
dispatched, which searches for the term within the list of registered
affiliates and updates the `affiliateItems` array in the Vuex store.
A watcher is activated in order to detect a change in this array.

If a change is detected and the array has elements, `found` is set to
true and then the child component `<AffiliatesTable>` is rendered. The
`<AffiliatesTable>` component shows a list of the affiliates resulting
from the search. If the array is empty, `found` is set to false and the
not found alert message is displayed.

When the `beforeDestroy` lifecycle hook is run, `affiliateItems` watching
is stopped.

**Syntax**

```typescript
affiliateSearch(term: string): void
```

**Parameters**

- `term: string`<br>
  Term to search

