# NewCrew

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<NewCrew>` es usado para desplegar un formulario
para la creación de nuevas tripulaciones.

Se puede acceder a este componente desde el botón flotante que
aparece en el componente `<CrewList>`.

---

The `<NewCrew>` component is used to display a form
for the creation of new crews.

You can access this component from the floating button that
appears in the `<CrewList>` component.

## Data

| Name                | Type      | Description | Initial value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ------------------- | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `watch`             | `object`  |             | `{"name":{"type":"string","value":"","raw":"\"\""},"start_date":{"type":"Object","value":"this.getCurrentDatetime","raw":"this.getCurrentDatetime"},"start_time":{"type":"BinaryExpression","value":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')","raw":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')"},"end_date":{"type":"Object","value":"this.getCurrentDatetime","raw":"this.getCurrentDatetime"},"end_time":{"type":"BinaryExpression","value":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')","raw":"String(new Date().getHours()).padStart(2, '0') + ':' +\n          String(new Date().getMinutes()).padStart(2, '0')"},"base":{"type":"string","value":"","raw":"\"\""},"mobile_unit":{"type":"string","value":"","raw":"\"\""},"practitioners":{"type":"array","value":"[]","raw":"[]"}}` |
| `shiftItems`        | `array`   |             | `[ {code: 1, display: 'Diurno'}, {code: 2, display: 'Nocturno'} ]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `dialog`            | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `snackbar`          | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `text`              | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `timeout`           | `number`  |             | `6000`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `color`             | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `calendarMenuStart` | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `calendarMenuEnd`   | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `watchMenuStart`    | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `watchMenuEnd`      | `boolean` |             | `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `doctor`            | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `paramedic`         | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `driver`            | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `shift`             | `string`  |             | `""`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

## Computed Properties

| Name                                 | Description                                                       |
| ------------------------------------ | ----------------------------------------------------------------- |
| `permissions`                        | **Dependencies:** `roles`                                         |
| `computedStartDateFormattedMomentjs` | **Dependencies:** `$i18n`, `watch`                                |
| `computedEndDateFormattedMomentjs`   | **Dependencies:** `$i18n`, `watch`                                |
| `getCurrentDatetime`                 | To use in new crew template<br>**Dependencies:** `formatDatetime` |
| `isDisabled`                         | **Dependencies:** `watch`, `driver`                               |

## Methods

### dateTimeFormat()

To use in new crew template

**Syntax**

```typescript
dateTimeFormat(date: unknow, time: unknow): unknow
```

### formatDatetime()

**Syntax**

```typescript
formatDatetime(datetime: unknow): unknow
```

### getPractitioners()

**Syntax**

```typescript
getPractitioners(): unknow
```

### handleSubmit()

**Syntax**

```typescript
handleSubmit(e: unknow): void
```

### resetCrewForm()

**Syntax**

```typescript
resetCrewForm(): void
```

### fullNamePractitioner()

**Syntax**

```typescript
fullNamePractitioner(item: unknow): unknow
```

### separatedNamePractitioner()

**Syntax**

```typescript
separatedNamePractitioner(item: unknow): unknow
```

