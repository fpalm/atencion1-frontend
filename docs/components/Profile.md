# Profile

**Author:** Ixhel Mejías, Adriana Tiso

El componente `<Profile>` se usa para mostrar la información del usuario.

En el componente se permite el cambio de contraseña del usuario para ingresar
a la aplicación.

Se puede acceder a este componente mediante el menú principal en `<NavBar>`.

---

The `<Profile>` component is used to display the user information.

The component allows to change the user's password to log in the application.

This component can be accessed through the main menu at `<NavBar>`.

## Data

| Name           | Type      | Description | Initial value  |
| -------------- | --------- | ----------- | -------------- |
| `text`         | `string`  |             | `""`           |
| `color`        | `string`  |             | `""`           |
| `timeout`      | `number`  |             | `0`            |
| `snackbar`     | `string`  |             | `""`           |
| `selected`     | `string`  |             | `""`           |
| `focusable`    | `boolean` |             | `true`         |
| `isEditing`    | `boolean` |             | `false`        |
| `pwEditing`    | `boolean` |             | `false`        |
| `newPassword1` | `string`  |             | `""`           |
| `newPassword2` | `string`  |             | `""`           |
| `first_name`   | `string`  |             | `""`           |
| `last_name`    | `string`  |             | `""`           |
| `email`        | `string`  |             | `""`           |
| `dni_type`     | `string`  |             | `""`           |
| `dni`          | `string`  |             | `""`           |
| `birthdate`    | `string`  |             | `""`           |
| `phoneNumbers` | `string`  |             | `""`           |
| `dniTypeItems` | `array`   |             | `[ 'V', 'E' ]` |

## Computed Properties

| Name         | Description                                      |
| ------------ | ------------------------------------------------ |
| `isDisabled` | **Dependencies:** `newPassword1`, `newPassword2` |

## Methods

### dateFormattedMomentjs()

**Syntax**

```typescript
dateFormattedMomentjs(date: unknow): unknow
```

### getPhoneNumbers()

**Syntax**

```typescript
getPhoneNumbers(phoneNumbers: unknow): unknow
```

### editProfile()

**Syntax**

```typescript
editProfile(): void
```

### saveProfileEdition()

**Syntax**

```typescript
saveProfileEdition(): void
```

### saveNewPassword()

**Syntax**

```typescript
saveNewPassword(): void
```

### resetNewPassword()

**Syntax**

```typescript
resetNewPassword(): void
```

