# Policies

**Author:** Ixhel Mejías

El componente `<Policies>` se utiliza para mostrar información acerca de las
pólizas de un afiliado: organización, plan y beneficiarios.

`<Policies>` es hijo del componente `<AffiliatesTable>`.

---

The `<Policies>` component is used to display information about an
affiliate's policies: organization, plan and beneficiaries.

`<Policies>` is child of the `<AffiliatesTable>` component.

## Props

| Name       | Type    | Description                                                                                                                           |
| ---------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| `policies` | `Array` | The array of objects with information for each policy corresponding to the selected affiliate from the `<AffiliatesTable>` component. |

## Data

| Name          | Type      | Description | Initial value                                                                                                                                                                                                                                              |
| ------------- | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `loading`     | `boolean` |             | `false`                                                                                                                                                                                                                                                    |
| `headers`     | `array`   |             | `[ { text: this.$t('first_name'), value: 'affiliate.first_name' }, { text: this.$t('last_name'), value: 'affiliate.last_name' }, { text: this.$t('idnumber'), value: 'affiliate.dni' }, { text: this.$t('linkage'), align: 'center', value: 'kinship' } ]` |
| `data`        | `array`   |             | `[]`                                                                                                                                                                                                                                                       |
| `isEditing`   | `any`     |             | `null`                                                                                                                                                                                                                                                     |
| `model`       | `any`     |             | `null`                                                                                                                                                                                                                                                     |
| `genderItems` | `array`   |             | `[ 'Masculino', 'Femenino' ]`                                                                                                                                                                                                                              |

## Computed Properties

| Name                    | Description                           |
| ----------------------- | ------------------------------------- |
| `fullNameAffiliate`     | **Dependencies:** `policies`          |
| `affiliateStatusColor`  | **Dependencies:** `policies`          |
| `dateFormattedMomentjs` | **Dependencies:** `$i18n`, `policies` |

## Methods

### getKinship()

The `getKinship` method translates the kinship to be displayed (from
english to spanish).

**Syntax**

```typescript
getKinship(kinship: string): string
```

**Parameters**

- `kinship: string`<br>
  Kinship to translate

**Return value**

Translated kinship

