# Introduction

Atención-1 es una aplicación web del Grupo Venemergencia que tiene como finalidad gestionar servicios de atención y emergencias médicas: Orientación Médica Telefónica (OMT), Atención Médica Domiciliaria (AMD), Traslados en Ambulancia, entre otros.

Atención-1 funciona utilizando:

- Django v2.1.1
- Django REST Framework v3.10.3
- PostgreSQL v.11.2
- Vue v.2.5.2
- Node.js v.6.0.0